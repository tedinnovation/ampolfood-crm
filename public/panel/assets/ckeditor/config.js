/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.width=780;
	config.height=300;
	//config.removeButtons = 'Underline,Subscript,Superscript';
	//config.filebrowserBrowseUrl = admin_url+'filemanager/';
	config.filebrowserImageUploadUrl = admin_url+'filemanager/upload';
	config.baseurl = base_url;
};
