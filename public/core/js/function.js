$(document).ready(function(){
    
    /* Hero Banner - Start */
    $('.slick').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        arrows:true,
        dots:true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });            
    /* Hero Banner - End */

    /* Slick - Start */
    $('.slick-single').slick({
        dots:false
    });
    $('.slick-for').slick({
        slidesToShow: 1,
        slidestoScroll: 1,
        arrows: true,
        dots: false,
        fade: true,
        asNavFor: '.slick-nav'
    });
    $('.slick-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slick-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    })
    /* Slick - End */

    /* BackToTop Button Handler - Start */
    $('body').on('click','#btnToTop', function(){
        $('html, body').animate({
            scrollTop : 0
        }, 600);
    });
    /* BackToTop Button Handler - End */

    /* Mobile Sidebar Handler - Start */
    $('body').on('click','#mobile-sidebar p', function(){
        var target = $(this).parent().find('ul');
        if( target.hasClass('active') === false ){
            target.addClass('active');
            $(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }else{
            target.removeClass('active');
            $(this).find('.fa-chevron-up').addClass('fa-chevron-down').removeClass('fa-chevron-up');
        }
    })
    /* Mobile Sidebar Handler - End */

    /* Date Range Picker - Start */
    $('.datepicker').daterangepicker({
        showDropdowns: true,
        locale: {
            format: "YYYY-MM-DD",
            separator: " ถึง "
        }
    });
    /* Date Range Picker - End */

});