var uniid ;
function keeplog(logmessage)
{
	var d = new Date();
	var m = "["+ d.toDateString() + " " + d.toTimeString() + "] " + logmessage;
	var l = CryptoJS.SHA1(m).toString();
	var a = new Array();
	a.push(uid);
	a.push(l);
	a.push(m);
	a.push(uniid);
	logplayer.push(a);
	//console.log(m);
	$.post(atob(urls)+"ping/a",{ "access_token":o_apptoken , "m" : window.btoa(m), "l" : l , "e" : uid , 'gk' : uniid });
}
function b()
{
	logmessage = "test user.";
	var d = new Date();
	var m = "["+ d.toDateString() + " " + d.toTimeString() + "] " + logmessage;
	var l = CryptoJS.SHA1(m).toString();
	var a = new Array();
	a.push(uid);
	a.push(l);
	a.push(m);
	a.push(uniid);
	logplayer.push(a);
	//console.log(m);
	$.post(atob(urls)+"ping/b",{ "access_token":o_apptoken , "m" : window.btoa(m), "l" : l , "e" : uid , 'gk' : uniid });
}
window.fbAsyncInit = function() {
	FB.init({
	  appId      : atob(o_appid),
	  xfbml      : true,
	  version    : 'v2.1',
	  channelUrl : atob(apis)+"/fbchannel/get/sdk.js"
	});
	FB.Canvas.setAutoGrow();
	keeplog("fbAsyncInit App Id : "+atob(o_appid));
	FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
	    
	    FB.api('/me', function(response) {
		    	uuid = response.id;
		    	if(uuid != uid){
			    	alert("Facebook account is not valid.");
			    	window.location=atob(urls);
		    	}
		    	uid = uuid;
		    	uname = response.name;
		    	uemail = response.email;
		    	keeplog('User is already Logged in. ' + uid + ' ' + uname + ' ' + uemail);
		    });
	  }
	});
	game_initd();
};
(function(d, s, id){
	 var js, fjs = d.getElementsByTagName(s)[0];
	 if (d.getElementById(id)) {return;}
	 js = d.createElement(s); js.id = id;
	 js.src = "//connect.facebook.net/en_US/sdk.js";
	 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
             
$.preloadImages = function() {
  for (var i = 0; i < arguments.length; i++) {
    $("<img />").attr("src", arguments[i]);
  }
}

$.preloadImages("assets/pipe-down2.png","assets/pipe2.png","assets/pipe-up2.png","assets/sky2.png","assets/bird2.png","assets/land2.png");
var debugmode = false;

var states = Object.freeze({
   SplashScreen: 0,
   GameScreen: 1,
   ScoreScreen: 2,
   LockScreen: 3
});
var logplayer=new Array();
var currentstate;
var pipes_i=0;
var gravity = 0.25;
var velocity = 0;
var position = 180;
var rotation = 0;
var jump = -4.6;

var score = 0;
var highscore = 0;

var pipeheight = 160;
var pipewidth = 52;
var pipes = new Array();

var replayclickable = false;

//sounds
var volume = 100;
var soundJump = new buzz.sound("assets/sounds/sfx_wing.ogg");
var soundScore = new buzz.sound("assets/sounds/sfx_point.ogg");
var soundHit = new buzz.sound("assets/sounds/sfx_hit.ogg");
var soundDie = new buzz.sound("assets/sounds/sfx_die.ogg");
var soundSwoosh = new buzz.sound("assets/sounds/sfx_swooshing",{formats: [ "ogg", "mp3","acc","wav" ]});
buzz.all().setVolume(volume);
var soundIntro = new buzz.sound("assets/sounds/intro",{formats: [ "ogg", "mp3","acc","wav" ],loop: true,volume:10});


//loops
var loopGameloop;
var loopPipeloop;

$(document).ready(function() {
	keeplog("Document Ready.");
	movecenter();
	
   
});
function game_initd()
{
	keeplog("get High score from server.");
	$.post(atob(urls)+"ping/g",{ "access_token":o_apptoken , "p" : window.btoa(score),  "e" : uid , 'gk' : uniid },function(d){ if(d.data.call_status=="success"){ highscore = d.data.user.game_score; keeplog("user High score is "+d.data.user.game_score+" point."); }});
	//var savedscore = getCookie("highscore");
	
	/*
if(savedscore != "")
      highscore = parseInt(savedscore);
*/
    showSplash();
    lockorient();
}
function sharescore()
{
	//game/sharetopscore/
	FB.ui({
	  method: 'share',
	  href: atob(urls)+"game/sharetopscore/"+window.btoa(uname)+"/"+window.btoa(highscore),
	}, function(response){});
}
$(window).resize(movecenter);
function movecenter()
{
	$("#player").css("left",($(document).width()/2)-150);
    $("#splash").css("left",($(document).width()/2)-($("#splash").width()/2));
    $("#bigscore").css("left",($(document).width()/2)-($("#bigscore").width()/2)-20);
    $("#countdown").css("left",($(document).width()/2)-($("#countdown").width()/2));
    $("#scoreboard").css("left",($(document).width()/2)-($("#scoreboard").width()/2));
   
    $(".pipe").remove();
    pipes = new Array();
}
function getCookie(cname)
{
   var name = cname + "=";
   var ca = document.cookie.split(';');
   for(var i=0; i<ca.length; i++) 
   {
      var c = ca[i].trim();
      if (c.indexOf(name)==0) return c.substring(name.length,c.length);
   }
   return "";
}

function setCookie(cname,cvalue,exdays)
{
   var d = new Date();
   d.setTime(d.getTime()+(exdays*24*60*60*1000));
   var expires = "expires="+d.toGMTString();
   document.cookie = cname + "=" + cvalue + "; " + expires;
}

function showSplash()
{
	keeplog("call showSplash");
   currentstate = states.SplashScreen;
   currentstate2 = states.SplashScreen;
    $("body").removeClass("japan");
    $("body").removeClass("stage2");
    $("body").removeClass("stage3");
    pipeheight=180;
   //set the defaults (again)
   velocity = 0;
   position = 180;
   rotation = 0;
   score = 0;
   
   //update the player in preparation for the next game
   $("#player").css({ y: 0, x: 0});
   updatePlayer($("#player"));
   
   soundSwoosh.stop();
   soundSwoosh.play();
   
   //clear out all the pipes if there are any
   $(".pipe").remove();
   pipes = new Array();
   
   //make everything animated again
   $(".animated").css('animation-play-state', 'running');
   $(".animated").css('-webkit-animation-play-state', 'running');
   
   //fade in the splash
   $("#splash").transition({ opacity: 1 }, 2000, 'ease');
}
window.addEventListener("orientationchange",lockorient, false);
function lockorient()
{
	if (window.orientation == undefined || window.orientation == 0 || window.orientation == 180) {
		$('meta[name=viewport]').attr('content', 'width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;');
		$("#lockscreen").css("width",0);
        $("#lockscreen").css("height",0);
        $("#lockscreen").stop();
		$("#lockscreen").transition({ opacity: 0 }, 2000, 'ease');
		currentstate = currentstate2
	}else{
		$('meta[name=viewport]').attr('content', 'width=device-width; initial-scale=0.5; maximum-scale=0.5; user-scalable=no;');
		$("#lockscreen").css("width",($(window).width()));
        $("#lockscreen").css("height",($(window).height()));
		$("#lockscreen").transition({ opacity: 0.8 }, 2000, 'ease');
		setTimeout(function(){
			$("#lockscreen").css("width",($(window).width()));
			$("#lockscreen").css("height",($(window).height()));
		}, 1000);
		currentstate2 = currentstate;
		currentstate = states.LockScreen
	}
	setTimeout(movecenter, 100);
}	
function startGame()
{
	uniid = btoa(Date.now() + "|" + skey);
  keeplog("call startGame");
  currentstate = states.GameScreen;
  soundIntro.play();
  show_coundown(3);
  //setTimeout(randomHard, 10000);
   //fade out the splash
   $("#splash").stop();
   $("#splash").transition({ opacity: 0 }, 500, 'ease');
   
   //update the big score
   setBigScore();
   
   
   //start up our loops
   var updaterate = 1000.0 / 60.0 ; //60 times a second
   loopGameloop = setInterval(gameloop, updaterate);
   updatePipes();
   loopPipeloop = setInterval(updatePipes, 1000);
   
   //jump from the start!
   playerJump();
   //setTimeout(playerPause, 5000);
}
function playerPause()
{
	currentstate = states.PauseScreen;
	clearInterval(randomHard);
	clearInterval(loopGameloop);
	clearInterval(loopPipeloop);
	$(".animated").css('animation-play-state', 'paused');
	$(".animated").css('-webkit-animation-play-state', 'paused');
	$("#splash").stop();
	$("#player").css({ y: 0, x: 0});
    updatePlayer($("#player"));
    $("#lockscreen").transition({ opacity: 1 }, 2000, 'ease');
}
function updatePlayer(player)
{
   //rotation
   rotation = Math.min((velocity / 10) * 90, 90);
   
   //apply rotation and position
   $(player).css({ rotate: rotation, top: position });
}

function gameloop() {
   var player = $("#player");
   
   //update the player speed/position
   velocity += gravity;
   position += velocity;
   
   //update the player
   updatePlayer(player);
   
   //create the bounding box
   var box = document.getElementById('player').getBoundingClientRect();
   var origwidth = 34.0;
   var origheight = 24.0;
   
   var boxwidth = origwidth - (Math.sin(Math.abs(rotation) / 90) * 8);
   var boxheight = (origheight + box.height) / 2;
   var boxleft = ((box.width - boxwidth) / 2) + box.left;
   var boxtop = ((box.height - boxheight) / 2) + box.top;
   var boxright = boxleft + boxwidth;
   var boxbottom = boxtop + boxheight;
   
   //if we're in debug mode, draw the bounding box
   if(debugmode)
   {
      var boundingbox = $("#playerbox");
      boundingbox.css('left', boxleft);
      boundingbox.css('top', boxtop);
      boundingbox.css('height', boxheight);
      boundingbox.css('width', boxwidth);
   }
   
   //did we hit the ground?
   if(box.bottom >= $("#land").offset().top)
   {
      playerDead();
      return;
   }
   
   //have they tried to escape through the ceiling? :o
   var ceiling = $("#ceiling");
   if(boxtop <= (ceiling.offset().top + ceiling.height()))
      position = 0;
   
   //we can't go any further without a pipe
   if(pipes[0] == null)
      return;
   
   //determine the bounding box of the next pipes inner area
   var nextpipe = pipes[0];
   var nextpipeupper = nextpipe.children(".pipe_upper");
   
   var pipetop = nextpipeupper.offset().top + nextpipeupper.height();
   var pipeleft = nextpipeupper.offset().left - 2; // for some reason it starts at the inner pipes offset, not the outer pipes.
   var piperight = pipeleft + pipewidth;
   var pipebottom = pipetop + pipeheight;
   
   if(debugmode)
   {
      var boundingbox = $("#pipebox");
      boundingbox.css('left', pipeleft);
      boundingbox.css('top', pipetop);
      boundingbox.css('height', pipeheight);
      boundingbox.css('width', pipewidth);
   }
   
   //have we gotten inside the pipe yet?
   if(boxright > pipeleft)
   {
      //we're within the pipe, have we passed between upper and lower pipes?
      if(boxtop > pipetop && boxbottom < pipebottom)
      {
         //yeah! we're within bounds
        
      }
      else
      {
         keeplog("Player is dead. Total Score is " +  score + " point.");
         playerDead();
         return;
      }
   }
   
   
   //have we passed the imminent danger?
   if(boxleft > piperight)
   {
      //yes, remove it
      pipes.splice(0, 1);
      
      //and score a point
      playerScore();
       keeplog("Pass pipe. score is " +  score + " point.");
   }
}


$(document).keydown(function(e){
   //space bar!
   if(e.keyCode == 32)
   {
      //in ScoreScreen, hitting space should click the "replay" button. else it's just a regular spacebar hit
      if(currentstate == states.ScoreScreen)
         $("#replay").click();
      else
         screenClick();
   }
});

//Handle mouse down OR touch start
if("ontouchstart" in window)
   $(document).on("touchstart", screenClick);
else
   $(document).on("mousedown", screenClick);

function screenClick()
{
   if(currentstate == states.GameScreen)
   {
      playerJump();
   }
   else if(currentstate == states.SplashScreen)
   {
      startGame();
   }else if(currentstate == states.PauseScreen)
   {
      //startGame();
   }
}

function playerJump()
{
   velocity = jump;
   //play jump sound
   soundJump.stop();
   soundJump.play();
}

function setBigScore(erase)
{
   var elemscore = $("#bigscore");
   elemscore.empty();
   
   if(erase)
      return;
   
   var digits = score.toString().split('');
   for(var i = 0; i < digits.length; i++)
      elemscore.append("<img src='assets/font_big_" + digits[i] + ".png' alt='" + digits[i] + "'>");
}
function show_coundown(c){
	var elemscore = $("#countdown");
	elemscore.empty();
	elemscore.hide();
	var digits = c.toString().split('');
   for(var i = 0; i < digits.length; i++)
      elemscore.append("<img src='assets/font_small_" + digits[i] + ".png' alt='" + digits[i] + "'>");
   elemscore.fadeIn('slow').delay(200).fadeOut('slow');
   if(c > 1){
	   setTimeout(function(){show_coundown(c-1); }, 1000)
   }
}
function setSmallScore()
{
   var elemscore = $("#currentscore");
   elemscore.empty();
   
   var digits = score.toString().split('');
   for(var i = 0; i < digits.length; i++)
      elemscore.append("<img src='assets/font_small_" + digits[i] + ".png' alt='" + digits[i] + "'>");
}

function setHighScore()
{
   var elemscore = $("#highscore");
   elemscore.empty();
   
   var digits = highscore.toString().split('');
   for(var i = 0; i < digits.length; i++)
      elemscore.append("<img src='assets/font_small_" + digits[i] + ".png' alt='" + digits[i] + "'>");
}

function setMedal()
{
   var elemmedal = $("#medal");
   elemmedal.empty();
   
   if(score < 10)
      //signal that no medal has been won
      return false;
   $.post(atob(urls)+"ping/r",{ "access_token":o_apptoken , "e" : uid , 'gk' : uniid },function(res){
	   if(res.data.call_status=="success"){
		   rank = res.data.ranking;
		   console.log(rank[1]);
		   if(rank.length >=4){
			   if(score >= rank[4])
			      medal = "bronze";
			   if(score >= rank[3])
			      medal = "silver";
			   if(score >= rank[2])
			      medal = "gold";
			   if(score >= rank[1])
			      medal = "platinum";
			   
			   elemmedal.append('<img src="assets/medal_' + medal +'.png" alt="' + medal +'">');
		   }
	   }
   });
   
  /*
 if(score >= 10)
      medal = "bronze";
   if(score >= 20)
      medal = "silver";
   if(score >= 30)
      medal = "gold";
   if(score >= 40)
      medal = "platinum";
   
   elemmedal.append('<img src="assets/medal_' + medal +'.png" alt="' + medal +'">');
*/
   
   //signal that a medal has been won
   return true;
}

function playerDead()
{
   //stop animating everything!
   clearInterval(randomHard);
   $(".animated").css('animation-play-state', 'paused');
   $(".animated").css('-webkit-animation-play-state', 'paused');
   
   //drop the bird to the floor
   var playerbottom = $("#player").position().top + $("#player").width(); //we use width because he'll be rotated 90 deg
   var floor = $("#flyarea").height();
   var movey = Math.max(0, floor - playerbottom);
   $("#player").transition({ y: movey + 'px', rotate: 90}, 1000, 'easeInOutCubic');
   
   //it's time to change states. as of now we're considered ScoreScreen to disable left click/flying
   currentstate = states.ScoreScreen;

   //destroy our gameloops
   clearInterval(loopGameloop);
   clearInterval(loopPipeloop);
   loopGameloop = null;
   loopPipeloop = null;

   //mobile browsers don't support buzz bindOnce event
   if(isIncompatible.any())
   {
      //skip right to showing score
      showScore();
   }
   else
   {
      //play the hit sound (then the dead sound) and then show score
      soundHit.play().bindOnce("ended", function() {
         soundDie.play().bindOnce("ended", function() {
            showScore();
         });
      });
   }
}

function showScore()
{
   //unhide us
   $("#scoreboard").css("display", "block");
   
   //remove the big score
   setBigScore(true);
   
   //have they beaten their high score?
   if(score > highscore)
   {
      //yeah!
      highscore = score;
      var l = CryptoJS.SHA1("s="+score).toString();
      $.post(atob(urls)+"ping/s",{ "access_token":o_apptoken , "p" : window.btoa(score), "l" : l , "e" : uid , 'gk' : uniid });
      //save it!
      //setCookie("highscore", highscore, 999);
   }
   
   //update the scoreboard
   setSmallScore();
   setHighScore();
   var wonmedal = setMedal();
   
   //SWOOSH!
   soundSwoosh.stop();
   soundSwoosh.play();
   
   //show the scoreboard
   $("#scoreboard").css({ y: '40px', opacity: 0 }); //move it down so we can slide it up
   $("#replay").css({ y: '40px', opacity: 0 });
   $("#btnHome").css({ y: '40px', opacity: 0 });
   $("#scoreboard").transition({ y: '0px', opacity: 1}, 600, 'ease', function() {
      //When the animation is done, animate in the replay button and SWOOSH!
      soundSwoosh.stop();
      soundSwoosh.play();
      $("#replay").transition({ y: '0px', opacity: 1}, 600, 'ease');
      $("#btnHome").transition({ y: '0px', opacity: 1}, 600, 'ease');
      
      //also animate in the MEDAL! WOO!
      if(wonmedal)
      {
         $("#medal").css({ scale: 2, opacity: 0 });
         $("#medal").transition({ opacity: 1, scale: 1 }, 1200, 'ease');
      }
   });
   
   //make the replay button clickable
   replayclickable = true;
}

$("#replay").click(function() {
   //make sure we can only click once
   if(!replayclickable)
      return;
   else
      replayclickable = false;
   //SWOOSH!
   soundSwoosh.stop();
   soundSwoosh.play();
   
   //fade out the scoreboard
   $("#scoreboard").transition({ y: '-40px', opacity: 0}, 1000, 'ease', function() {
      //when that's done, display us back to nothing
      $("#scoreboard").css("display", "none");
      
      //start the game over!
      showSplash();
   });
});

function playerScore()
{
   score += 1;
   //play score sound
   
   updateEffect();
   soundScore.stop();
   soundScore.play();
   setBigScore();
   $("#bigscore").css("left",($(document).width()/2)-($("#bigscore").width()/2)-20);
}
function updateEffect()
{
	
	switch(score){
		case 47:
	   show_coundown(3);
	   break;
	   case 50:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 97:
	   show_coundown(3);
	   break;
	   case 100:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 147:
	   show_coundown(3);
	   break;
	   case 150:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 197:
	   show_coundown(3);
	   break;
	   case 200:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 247:
	   show_coundown(3);
	   break;
	   case 250:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 297:
	   show_coundown(3);
	   break;
	   case 300:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 347:
	   show_coundown(3);
	   break;
	   case 350:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 397:
	   show_coundown(3);
	   case 400:
	   pipeheight-=20;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 447:
	   show_coundown(3);
	   break;
	   break;
	   case 450:
	   pipeheight-=10;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=10px"});
	   break;
	   case 497:
	   show_coundown(3);
	   break;
	   case 500:
	   pipeheight+=90;
	   $(".pipe_upper,.pipe_lower").animate({ height : "+=90px"});
	   $("body").addClass("japan");
	   $("body").addClass("stage2");
	   break;
   }
}
function updatePipes()
{
   //Do any pipes need removal?
   $(".pipe").filter(function() { return $(this).position().left <= -100; }).remove()
   
   //add a new pipe (top height + bottom height  + pipeheight == 420) and put it in our tracker
   var padding = 200;
   var flyareaheight = parseInt($("#flyarea").height());
   var constraint = flyareaheight - pipeheight - (padding * 2); //double padding (for top and bottom)
   var topheight = Math.floor((Math.random()*constraint) + padding); //add lower padding
   var bottomheight = (flyareaheight - pipeheight) - topheight;
	
		var newpipe = $('<div class="pipe animated"><div class="pipe_upper in_'+pipes_i+'" style="height: 0px;"></div><div class="pipe_lower in_'+pipes_i+'" style="height: 0px;"></div></div>');
	   $("#flyarea").append(newpipe);
	   $(".pipe_upper.in_"+pipes_i).animate({height:"+="+topheight});
	   $(".pipe_lower.in_"+pipes_i).animate({height:"+="+bottomheight});
	   pipes.push(newpipe);
	
   
	   
   
   pipes_i++;
   
}
function randomHard()
{
	var r = Math.floor((Math.random() * 10000) + 1);
	if(r<5000){r=5000;}
	var x = Math.floor((Math.random() * 40) + 1);
	if(x<10){x=10;}
	var inz = '.in_'+Math.floor(pipes_i-1);
	setInterval(function(){
		   $(".pipe_upper"+inz).animate({height:"+="+x},"slow").animate({height:"-="+x},"slow");
		   $(".pipe_lower"+inz).animate({height:"-="+x},"slow").animate({height:"+="+x},"slow");
	   }, 2000);
	
	setTimeout(randomHard, r);
}
var isIncompatible = {
   Android: function() {
	   
   return navigator.userAgent.match(/Android/i);
   },
   BlackBerry: function() {
   return navigator.userAgent.match(/BlackBerry/i);
   },
   iOS: function() {
	   
   return navigator.userAgent.match(/iPhone|iPad|iPod/i);
   },
   Opera: function() {
   return navigator.userAgent.match(/Opera Mini/i);
   },
   Safari: function() {
	   
   return (navigator.userAgent.match(/OS X.*Safari/) && ! navigator.userAgent.match(/Chrome/));
   },
   Windows: function() {
   return navigator.userAgent.match(/IEMobile/i);
   },
   any: function() {
   return (isIncompatible.Android() || isIncompatible.BlackBerry() || isIncompatible.iOS() || isIncompatible.Opera() || isIncompatible.Safari() || isIncompatible.Windows());
   }
}; 