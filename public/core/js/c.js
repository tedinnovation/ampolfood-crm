
function isInspectOpen()
{
    console.profile(); 
    console.profileEnd(); 
    if (console.clear) console.clear();
    return console.profiles.length > 0;
}
(function () {
	'use strict';
	var devtools = {open: false};
	var threshold = 160;
	var emitEvent = function (state) {
		window.dispatchEvent(new CustomEvent('devtoolschange', {
			detail: {
				open: state
			}
		}));
	};

	setInterval(function () {
		if ((window.Firebug && window.Firebug.chrome && window.Firebug.chrome.isInitialized) || isInspectOpen()) {
			if (!devtools.open) {
				emitEvent(true);
			}
			devtools.open = true;
		} else {
			if (devtools.open) {
				emitEvent(false);
			}
			devtools.open = false;
		}
	}, 500);

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = devtools;
	} else {
		window.devtools = devtools;
	}
})();
/*
( function(){
	var __x = Function.prototype.call;
        Function.prototype.call = function( thisArg ){
             if( arguments[1] && arguments[1].indexOf &&
                 arguments[1].indexOf( "with (__commandLineAPI" ) !== -1 ) {
			throw "Sorry, Execution via Console has been disabled!";
             }
 
	     __x.apply( this, arguments );
        };
})();
*/
window.addEventListener('devtoolschange', function (e) {
        if(e.detail.open==true){
	        //console.clear();
	        console.log("%cStop! This is a browser feature intended for developers. If someone told you to copy-paste something here to enable a feature or 'hack' someone's account, it is a scam and will give them access to your account..", "font: 2em sans-serif; color: yellow; background-color: red;");
	        keeplog("Hacked Console is enable. Log has been send to administrator.");
	        b()
        }
});
