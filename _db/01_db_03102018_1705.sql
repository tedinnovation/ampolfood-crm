create table if not exists `rwds_pts_order` (
    `id` bigInt auto_increment,
    `user_id`	bigInt not null default 0,
    `order_no`	varchar(255) null,
    `order_datetime` datetime null,
    `address_id` int(11) not null default 0,
    `order_total_qty` int(11) not null default 0,
    `order_shipping_fee` decimal(10,2) not null default 0,
    `order_grand_total`	decimal(10,2) not null default 0,
    `order_points_use`	int(11) not null default 0,
    `order_remark`	text null,
    `order_status`	enum('approved','pending','discard','ship') not null default 'pending',
    `created_at`	datetime null,
    `updated_by`	bigInt null,
    `updated_at`	datetime null,

    primary key (`id`),
            key `user` (`user_id`),
            key `address` (`address_id`)
)engine=MyISAM default charset=utf8;

create table if not exists `rwds_pts_order_items`(
    `id` bigInt auto_increment,
    `order_id` bigInt not null default 0,
    `user_id` bigInt not null default 0,
    `reward_id` bigInt not null default 0,
    `item_qty` int(11) not null default 0,
    `item_price` decimal(10,2) not null default 0,
    `item_total` decimal(10,2) not null default 0,
    `created_at` datetime null,
    `updated_by` datetime null,
    `updated_at` datetime null,

    primary key (`id`),
            key `order` (`order_id`),
            key `user` (`user_id`),
            key `reward` (`reward_id`)
)engine=MyISAM default charset=utf8;

create table if not exists `rwds_pts_logs`(
    `id` bigInt auto_increment,
    `reference_id` bigInt not null default 0,
    `logs_type` enum('add','minus') not null default 'minus',
    `logs_point` int(11) not null default 0,
    `logs_remark` text null,
    `created_at` datetime null,

    primary key (`id`),
            key `reference` (`reference_id`)
)engine=MyISAM default charset=utf8;

create table if not exists `rwds_rewards_category`(
    `id` bigInt auto_increment,
    `category_icon` varchar(255) null,
    `category_name_th` varchar(255) null,
    `category_name_en` varchar(255) null,
    `category_order` int(1),
    `category_status` enum('approved','pending','discard') not null default 'approved',
    `created_by` bigInt null,
    `created_at` datetime null,
    `updated_by` bigInt null,

    primary key (`id`)
)engine=MyISAM default charset=utf8;

create table if not exists `rwds_rewards`(
    `id` bigInt auto_increment,
    `category_id` bigInt not null default 0,
    `rewards_thumbnail` varchar(255) null,
    `rewards_image` varchar(255) null,
    `rewards_name_th` varchar(255) null,
    `rewards_name_en` varchar(255) null,
    `rewards_sdesc_th` text null,
    `rewards_sdesc_en` text null,
    `rewards_desc_th` text null,
    `rewards_desc_en` text null,
    `rewards_hilight_status` int(1) not null default 0,
    `rewards_status` enum('approved','pending','discard') not null default 'approved',
    `created_by` bigInt null,
    `created_at` datetime null,
    `updated_by` bigInt null,
    `updated_at` datetime null,

    primary key (`id`),
            key `category` (`category_id`)
)engine=MyISAM default charset=utf8;