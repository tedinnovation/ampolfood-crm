/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : ci_master_v3

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-03-28 18:45:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for rwds_system_company
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_company`;
CREATE TABLE `rwds_system_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_logo` varchar(255) NULL,
  `company_name` varchar(40) DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `join_ip` varchar(15) DEFAULT NULL,
  `company_status` enum('active','suspend','delete') DEFAULT 'active',
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_company
-- ----------------------------
INSERT INTO `rwds_system_company` VALUES ('1','','TED Innovation Co., Ltd.', '2013-01-26 11:41:14', '127.0.0.1', 'active');

-- ----------------------------
-- Table structure for rwds_system_fbusers
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_fbusers`;
CREATE TABLE `rwds_system_fbusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(20) DEFAULT NULL,
  `fbname` varchar(50) DEFAULT NULL,
  `fbfname` varchar(50) DEFAULT NULL,
  `fblname` varchar(50) DEFAULT NULL,
  `fbemail` varchar(50) DEFAULT NULL,
  `game_score_date` datetime DEFAULT NULL,
  `game_score` bigint(20) unsigned DEFAULT '0',
  `status` enum('authorize','deauthorize','deleted') DEFAULT 'authorize',
  `is_hunters_player` enum('no','yes') DEFAULT 'no',
  `fbgender` enum('male','female') DEFAULT NULL,
  `fbbirthday` date DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `join_ip` char(15) DEFAULT '000.000.000.000',
  `lastlogin_date` datetime DEFAULT NULL,
  `lastlogin_ip` char(15) DEFAULT '000.000.000.000',
  `access_token` varchar(255) DEFAULT NULL,
  `profile_image` text,
  `profile_status` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fbid` (`fbid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Facebook Application Users';

-- ----------------------------
-- Records of rwds_system_fbusers
-- ----------------------------

-- ----------------------------
-- Table structure for rwds_system_group
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_group`;
CREATE TABLE `rwds_system_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `group_superadmin` enum('yes','no') DEFAULT 'no',
  `group_status` enum('active','suspend','delete') DEFAULT 'suspend',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_group
-- ----------------------------
INSERT INTO `rwds_system_group` VALUES ('1', '1', 'Super Administrator', 'yes', 'active');
INSERT INTO `rwds_system_group` VALUES ('2', '1', 'Manager', 'no', 'active');
INSERT INTO `rwds_system_group` VALUES ('3', '1', 'Moderator', 'no', 'active');
INSERT INTO `rwds_system_group` VALUES ('4', '1', 'Administrator', 'no', 'active');

-- ----------------------------
-- Table structure for rwds_system_language
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_language`;
CREATE TABLE `rwds_system_language` (
  `lang_id` char(2) NOT NULL DEFAULT '',
  `lang_name` varchar(50) DEFAULT NULL,
  `lang_flag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_language
-- ----------------------------
INSERT INTO `rwds_system_language` VALUES ('TH', 'ภาษาไทย', 'th.png');
INSERT INTO `rwds_system_language` VALUES ('EN', 'English', 'us.png');

-- ----------------------------
-- Table structure for rwds_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_menu`;
CREATE TABLE `rwds_system_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_label` varchar(30) DEFAULT NULL,
  `menu_icon` varchar(20) DEFAULT NULL,
  `menu_link` varchar(100) DEFAULT NULL,
  `menu_sequent` int(11) DEFAULT '1',
  `menu_key` varchar(32) DEFAULT NULL,
  `is_label` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rwds_system_menu
-- ----------------------------
INSERT INTO `rwds_system_menu` VALUES ('1', 'ภาพรวมระบบ', 'icon-laptop', 'dashboard', '0', 'd41d8cd98f00b204e9800998ecf8427e', 'no');
INSERT INTO `rwds_system_menu` VALUES ('2', 'การตั้งค่า', 'icon-gears', 'user_group', '900', '5e975624c5a4e4cb9fddde6da3bda19f', 'no');

-- ----------------------------
-- Table structure for rwds_system_permision
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_permision`;
CREATE TABLE `rwds_system_permision` (
  `perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `perm_status` enum('active','suspend','delete') DEFAULT 'active',
  PRIMARY KEY (`perm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_permision
-- ----------------------------
INSERT INTO `rwds_system_permision` VALUES ('70', '2', '2', '6', 'active');
INSERT INTO `rwds_system_permision` VALUES ('69', '2', '2', '5', 'active');
INSERT INTO `rwds_system_permision` VALUES ('68', '2', '2', '2', 'active');
INSERT INTO `rwds_system_permision` VALUES ('67', '2', '2', '1', 'active');
INSERT INTO `rwds_system_permision` VALUES ('88', '4', '67', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('87', '4', '66', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('86', '4', '65', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('35', '3', '3', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('34', '3', '1', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('66', '2', '3', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('65', '2', '1', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('85', '4', '64', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('84', '4', '1', '0', 'active');
INSERT INTO `rwds_system_permision` VALUES ('89', '4', '63', '0', 'active');

-- ----------------------------
-- Table structure for rwds_system_province
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_province`;
CREATE TABLE `rwds_system_province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_code` varchar(2) NOT NULL DEFAULT '0',
  `province_name_th` varchar(255) NOT NULL DEFAULT 'Province name (th)',
  `province_name_en` varchar(255) NOT NULL DEFAULT 'Province name (en)',
  `region_id` int(11) NOT NULL DEFAULT '0',
  `special_zone` int(11) NOT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_province
-- ----------------------------
INSERT INTO `rwds_system_province` VALUES ('1', '10', 'กรุงเทพมหานคร', 'Bangkok', '7', '1');
INSERT INTO `rwds_system_province` VALUES ('2', '11', 'สมุทรปราการ', 'Samut Prakan', '7', '1');
INSERT INTO `rwds_system_province` VALUES ('3', '12', 'นนทบุรี', 'Nonthaburi', '7', '1');
INSERT INTO `rwds_system_province` VALUES ('4', '13', 'ปทุมธานี', 'Pathum Thani', '7', '1');
INSERT INTO `rwds_system_province` VALUES ('5', '14', 'พระนครศรีอยุธยา', 'Phra Nakhon Si Ayutthaya', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('6', '15', 'อ่างทอง', 'AngThong', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('7', '16', 'ลพบุรี', 'LopBuri', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('8', '17', 'สิงห์บุรี', 'SingBuri', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('9', '18', 'ชัยนาท', 'ChaiNat', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('10', '19', 'สระบุรี', 'SaraBuri', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('11', '20', 'ชลบุรี', 'ChonBuri', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('12', '21', 'ระยอง', 'Rayong', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('13', '22', 'จันทบุรี', 'Chanthaburi', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('14', '23', 'ตราด', 'Trat', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('15', '24', 'ฉะเชิงเทรา', 'Chachoengsao', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('16', '25', 'ปราจีนบุรี', 'PrachinBuri', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('17', '26', 'นครนายก', 'NakhonNayok', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('18', '27', 'สระแก้ว', 'SaKaeo', '5', '0');
INSERT INTO `rwds_system_province` VALUES ('19', '30', 'นครราชสีมา', 'NakhonRatchasima', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('20', '31', 'บุรีรัมย์', 'BuriRam', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('21', '32', 'สุรินทร์', 'Surin', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('22', '33', 'ศรีสะเกษ', 'SiSaKet', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('23', '34', 'อุบลราชธานี', 'UbonRatchathani', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('24', '35', 'ยโสธร', 'Yasothon', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('25', '36', 'ชัยภูมิ', 'Chaiyaphum', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('26', '37', 'อำนาจเจริญ', 'AmnatCharoen', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('27', '39', 'หนองบัวลำภู', 'NongBuaLamPhu', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('28', '40', 'ขอนแก่น', 'KhonKaen', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('29', '41', 'อุดรธานี', 'UdonThani', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('30', '42', 'เลย', 'Loei', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('31', '43', 'หนองคาย', 'NongKhai', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('32', '44', 'มหาสารคาม', 'MahaSarakham', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('33', '45', 'ร้อยเอ็ด', 'RoiEt', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('34', '46', 'กาฬสินธุ์', 'Kalasin', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('35', '47', 'สกลนคร', 'SakonNakhon', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('36', '48', 'นครพนม', 'NakhonPhanom', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('37', '49', 'มุกดาหาร', 'Mukdahan', '3', '0');
INSERT INTO `rwds_system_province` VALUES ('38', '50', 'เชียงใหม่', 'ChiangMai', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('39', '51', 'ลำพูน', 'Lamphun', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('40', '52', 'ลำปาง', 'Lampang', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('41', '53', 'อุตรดิตถ์', 'Uttaradit', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('42', '54', 'แพร่', 'Phrae', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('43', '55', 'น่าน', 'Nan', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('44', '56', 'พะเยา', 'Phayao', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('45', '57', 'เชียงราย', 'ChiangRai', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('46', '58', 'แม่ฮ่องสอน', 'MaeHongSon', '1', '0');
INSERT INTO `rwds_system_province` VALUES ('47', '60', 'นครสวรรค์', 'NakhonSawan', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('48', '61', 'อุทัยธานี', 'UthaiThani', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('49', '62', 'กำแพงเพชร', 'Kamphaeng Phet', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('50', '63', 'ตาก', 'Tak', '4', '0');
INSERT INTO `rwds_system_province` VALUES ('51', '64', 'สุโขทัย', 'Sukhothai', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('52', '65', 'พิษณุโลก', 'Phitsanulok', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('53', '66', 'พิจิตร', 'Phichit', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('54', '67', 'เพชรบูรณ์', 'Phetchabun', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('55', '70', 'ราชบุรี', 'Ratchaburi', '4', '0');
INSERT INTO `rwds_system_province` VALUES ('56', '71', 'กาญจนบุรี', 'KanchanaBuri', '4', '0');
INSERT INTO `rwds_system_province` VALUES ('57', '72', 'สุพรรณบุรี', 'SuphanBuri', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('58', '73', 'นครปฐม', 'NakhonPathom', '7', '1');
INSERT INTO `rwds_system_province` VALUES ('59', '74', 'สมุทรสาคร', 'SamutSakhon', '7', '1');
INSERT INTO `rwds_system_province` VALUES ('60', '75', 'สมุทรสงคราม', 'SamutSongkhram', '2', '0');
INSERT INTO `rwds_system_province` VALUES ('61', '76', 'เพชรบุรี', 'Phetchaburi', '4', '0');
INSERT INTO `rwds_system_province` VALUES ('62', '77', 'ประจวบคีรีขันธ์', 'PrachinBuri', '4', '0');
INSERT INTO `rwds_system_province` VALUES ('63', '80', 'นครศรีธรรมราช', 'PhraNakhonSiAyutthaya', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('64', '81', 'กระบี่', 'Krabi', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('65', '82', 'พังงา', 'Phangnga', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('66', '83', 'ภูเก็ต', 'Phuket', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('67', '84', 'สุราษฎร์ธานี', 'SuratThani', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('68', '85', 'ระนอง', 'Ranong', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('69', '86', 'ชุมพร', 'Chumphon', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('70', '90', 'สงขลา', 'Songkhla', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('71', '91', 'สตูล', 'Satun', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('72', '92', 'ตรัง', 'Trang', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('73', '93', 'พัทลุง', 'Phatthalung', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('74', '94', 'ปัตตานี', 'Pattani', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('75', '95', 'ยะลา', 'Yala', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('76', '96', 'นราธิวาส', 'Narathiwat', '6', '0');
INSERT INTO `rwds_system_province` VALUES ('77', '38', 'บึงกาฬ', 'Buengkan', '3', '0');

-- ----------------------------
-- Table structure for rwds_system_region
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_region`;
CREATE TABLE `rwds_system_region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region_th` varchar(200) DEFAULT NULL,
  `region_en` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_region
-- ----------------------------
INSERT INTO `rwds_system_region` VALUES ('1', 'ภาคเหนือ', 'Northern');
INSERT INTO `rwds_system_region` VALUES ('2', 'ภาคกลาง', 'Central region');
INSERT INTO `rwds_system_region` VALUES ('3', 'ภาคตะวันออกเฉียงเหนือ ', 'Northeast');
INSERT INTO `rwds_system_region` VALUES ('4', 'ภาคตะวันตก  ', 'Western');
INSERT INTO `rwds_system_region` VALUES ('5', 'ภาคตะวันออก', 'Southern');
INSERT INTO `rwds_system_region` VALUES ('6', 'ภาคใต้ ', 'Southern');
INSERT INTO `rwds_system_region` VALUES ('7', 'กรุงเทพฯและปริมณฑล', 'Bangkok and its vicinity');

-- ----------------------------
-- Table structure for rwds_system_resetpass
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_resetpass`;
CREATE TABLE `rwds_system_resetpass` (
  `reset_id` int(11) NOT NULL AUTO_INCREMENT,
  `reset_key` char(36) NOT NULL DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `reset_date` datetime DEFAULT NULL,
  `reset_ip` varchar(15) DEFAULT NULL,
  `reset_status` enum('pending','complete','expired') DEFAULT 'pending',
  PRIMARY KEY (`reset_id`,`reset_key`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_resetpass
-- ----------------------------
INSERT INTO `rwds_system_resetpass` VALUES ('1', '1fdc7e9c-6315-11e2-8c2a-bc305bdf7c30', '1', '2013-01-20 22:21:55', '14.207.41.193', 'expired');
INSERT INTO `rwds_system_resetpass` VALUES ('2', '56db6d38-631d-11e2-8c2a-bc305bdf7c30', '1', '2013-01-20 23:20:43', '14.207.41.193', 'expired');
INSERT INTO `rwds_system_resetpass` VALUES ('3', '3eeab478-637b-11e2-8c2a-bc305bdf7c30', '1', '2013-01-21 10:32:56', '110.77.229.120', 'expired');

-- ----------------------------
-- Table structure for rwds_system_sessions
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_sessions`;
CREATE TABLE `rwds_system_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of rwds_system_sessions
-- ----------------------------
INSERT INTO `rwds_system_sessions` VALUES ('04cc91a0220c6284d0547fd846a99fb0', '118.174.57.42', 'Mozilla/5.0 (Linux; U; Android 4.1.2; th-th; GT-P6800 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 S', '1424789329', '');
INSERT INTO `rwds_system_sessions` VALUES ('0a0b7e99aa5c527105cae12b85e2d623', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424783029', '');
INSERT INTO `rwds_system_sessions` VALUES ('10da556ead8a03736ad75d7a7d9346fd', '27.145.87.174', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36', '1424828919', '');
INSERT INTO `rwds_system_sessions` VALUES ('1533f986c5eaeca641e81f1d76aff276', '171.100.35.79', 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B43', '1424781960', '');
INSERT INTO `rwds_system_sessions` VALUES ('164352f24c00c865a65d818c618ad2b6', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424783029', '');
INSERT INTO `rwds_system_sessions` VALUES ('1b96f7693863f2b1d5bdefd4bb8875ae', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424783029', '');
INSERT INTO `rwds_system_sessions` VALUES ('21120acb59a1948e6d8b35229d6e05b1', '66.249.65.198', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424801583', '');
INSERT INTO `rwds_system_sessions` VALUES ('21cbeffdb837135644a542df9a23cfc5', '66.249.65.130', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424804155', '');
INSERT INTO `rwds_system_sessions` VALUES ('2263e8cf1ccb798f49ec0cd290920f28', '66.249.73.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424801327', '');
INSERT INTO `rwds_system_sessions` VALUES ('24dd4a8e5ba86e98e39f8c927e34e9cd', '1.47.200.25', 'Mozilla/5.0 (compatible; Genieo/1.0 http://www.genieo.com/webfilter.html)', '1424829144', '');
INSERT INTO `rwds_system_sessions` VALUES ('29c5a9bfac257f3002b07db76b9d186d', '58.137.124.34', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36', '1424832491', '');
INSERT INTO `rwds_system_sessions` VALUES ('2a143a76ffe2341ddf905c3c5639a3e3', '66.249.65.134', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424810069', '');
INSERT INTO `rwds_system_sessions` VALUES ('420ebe97d75d8bf3b96936a6020ae64c', '66.249.73.243', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424826353', '');
INSERT INTO `rwds_system_sessions` VALUES ('45c1c7b6c39c7ad08f60ee3392ce2d86', '66.249.73.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424786451', '');
INSERT INTO `rwds_system_sessions` VALUES ('518d63f1b0e0ab8d42a85ef7ac2f348d', '171.96.181.98', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36', '1424798200', '');
INSERT INTO `rwds_system_sessions` VALUES ('52ec4cac678a51f898365af1499c7438', '109.58.144.193', 'Mozilla/5.0 (Linux; U; Android 4.3; sv-se; GT-I9300 Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mob', '1424777514', '');
INSERT INTO `rwds_system_sessions` VALUES ('54fe39b1cba43db0c563f6bfe46a93d1', '66.249.73.131', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', '1424775594', '');
INSERT INTO `rwds_system_sessions` VALUES ('556f24a7b96c7e63cca2a693e4b64dee', '66.249.73.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424802320', '');
INSERT INTO `rwds_system_sessions` VALUES ('55ad812d0b4c678ba7914ba047e3e278', '207.46.13.57', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', '1424833133', '');
INSERT INTO `rwds_system_sessions` VALUES ('5873a16671c95c722e8ff2665f0f4cb2', '1.47.7.24', 'Mozilla/5.0 (Linux; Android 4.1.2; GT-N7100 Build/JZO54K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.109 Mo', '1424788402', '');
INSERT INTO `rwds_system_sessions` VALUES ('591cc760449b90a066de4cef4d9189e4', '66.249.65.130', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424804412', '');
INSERT INTO `rwds_system_sessions` VALUES ('59c6b1535c1f2f1df330e814bf1f4f32', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424783029', '');
INSERT INTO `rwds_system_sessions` VALUES ('5c4ac30f04859de4892ce50771304cff', '207.46.13.57', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', '1424801933', '');
INSERT INTO `rwds_system_sessions` VALUES ('675afbc8fbbc816c0248279c28dc70c8', '49.230.152.239', 'Mozilla/5.0 (Linux; U; Android 4.1.2; th-th; GT-I8262 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 M', '1424813023', '');
INSERT INTO `rwds_system_sessions` VALUES ('69e52c3803cbb833260c261603a4acff', '171.96.181.98', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36', '1424798199', '');
INSERT INTO `rwds_system_sessions` VALUES ('70fcc495c8485fa56f86a6162714302d', '66.249.65.134', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424786901', '');
INSERT INTO `rwds_system_sessions` VALUES ('7195f5393ab650c0d36f882c4f3979aa', '66.249.65.198', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424832523', '');
INSERT INTO `rwds_system_sessions` VALUES ('7ae173b6c2871939ce6fd0087797d75c', '66.249.65.130', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424775201', '');
INSERT INTO `rwds_system_sessions` VALUES ('80b7a3fb36ccd94be7419b149556537a', '64.79.85.205', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko)                 Version/6.0', '1424782844', '');
INSERT INTO `rwds_system_sessions` VALUES ('84e73024d3329b24df5b8b5c54422be2', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424782812', '');
INSERT INTO `rwds_system_sessions` VALUES ('853ac370ab43648253d5153f4a75f2ec', '171.96.181.98', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36', '1424798200', '');
INSERT INTO `rwds_system_sessions` VALUES ('8fcca8fc54c3fb680c4964bf4311531b', '118.172.144.13', 'Mozilla/5.0 (compatible; Genieo/1.0 http://www.genieo.com/webfilter.html)', '1424789001', '');
INSERT INTO `rwds_system_sessions` VALUES ('90207a067920432e0f7b2ce8e91554f6', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424782940', '');
INSERT INTO `rwds_system_sessions` VALUES ('98a103bf47b2913efbf07db5cc70d4d0', '110.168.229.7', 'Mozilla/5.0 (iPad; CPU OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A365 [FBAN/FBIOS;FBAV/20.1', '1424781439', '');
INSERT INTO `rwds_system_sessions` VALUES ('9f89c0f0f6b79c3072d540c89d54938c', '66.249.84.220', 'Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20110814 Firefox/6.0 Google favicon', '1424780403', '');
INSERT INTO `rwds_system_sessions` VALUES ('a026990d9a4dbad4438e4e453e342b2f', '110.77.216.29', 'Mozilla/5.0 (compatible; Genieo/1.0 http://www.genieo.com/webfilter.html)', '1424787695', '');
INSERT INTO `rwds_system_sessions` VALUES ('a5ba6605a2943727b0f7849234887737', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424783029', '');
INSERT INTO `rwds_system_sessions` VALUES ('abdcb10a322b32f6a2d984567e7cc8fb', '66.249.65.198', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424796762', '');
INSERT INTO `rwds_system_sessions` VALUES ('acfd66f58189700f5bda04f65c798f25', '157.55.39.79', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', '1424812402', '');
INSERT INTO `rwds_system_sessions` VALUES ('b6d9e8edbd9930e40817c1f619129209', '1.47.233.198', 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B46', '1424778250', '');
INSERT INTO `rwds_system_sessions` VALUES ('c0724d290e7e3de98406ff3da12277bb', '171.5.183.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0', '1424780348', '');
INSERT INTO `rwds_system_sessions` VALUES ('d67e0cfcfbcb52f41927249e1a8aef48', '66.249.65.198', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', '1424799123', '');
INSERT INTO `rwds_system_sessions` VALUES ('d7a0b78991c2d85f7b7d03108ca74f5e', '66.249.84.226', 'Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20110814 Firefox/6.0 Google favicon', '1424827227', '');
INSERT INTO `rwds_system_sessions` VALUES ('da3c080e9fa1df1ac7a98ac15e9e42ce', '157.55.39.79', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', '1424801589', '');
INSERT INTO `rwds_system_sessions` VALUES ('dbc73ea6c86bf86a7602fce0ed12474d', '27.55.129.100', 'Mozilla/5.0 (compatible; Genieo/1.0 http://www.genieo.com/webfilter.html)', '1424797895', '');
INSERT INTO `rwds_system_sessions` VALUES ('e26065cac6b0d71e5f719c4497bc5881', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424782939', '');
INSERT INTO `rwds_system_sessions` VALUES ('ebf284e8baf6cd6f73e4b288c6f676e5', '64.79.85.205', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko)                 Version/6.0', '1424782904', '');
INSERT INTO `rwds_system_sessions` VALUES ('f8e1808016cf3dd86867247bfd93330b', '64.79.85.205', 'Mozilla/5.0 (compatible; SMTBot/1.0; +http://www.similartech.com/smtbot)', '1424783029', '');

-- ----------------------------
-- Table structure for rwds_system_setting
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_setting`;
CREATE TABLE `rwds_system_setting` (
  `setting_key` varchar(100) NOT NULL,
  `setting_value` text,
  PRIMARY KEY (`setting_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_setting
-- ----------------------------
INSERT INTO `rwds_system_setting` VALUES ('analytic_tracking_id', null);
INSERT INTO `rwds_system_setting` VALUES ('analytic_tracking_add_code', '');
INSERT INTO `rwds_system_setting` VALUES ('analytic_tracking_event', 'no');
INSERT INTO `rwds_system_setting` VALUES ('web_title', 'CIMB Thai Love Out Loud');
INSERT INTO `rwds_system_setting` VALUES ('web_keyword', null);
INSERT INTO `rwds_system_setting` VALUES ('web_head_add_code', 'fefe');
INSERT INTO `rwds_system_setting` VALUES ('since', 'ffc116ff784f9b2d93ddff632e9a380e6ecc48bd4ce091a91378b1bee940c9f22c03');

-- ----------------------------
-- Table structure for rwds_system_settings
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_settings`;
CREATE TABLE `rwds_system_settings` (
  `setting_id` varchar(50) NOT NULL,
  `setting_value` text,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of rwds_system_settings
-- ----------------------------

-- ----------------------------
-- Table structure for rwds_system_submenu
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_submenu`;
CREATE TABLE `rwds_system_submenu` (
  `submenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `menu_label` varchar(30) DEFAULT NULL,
  `menu_icon` varchar(30) DEFAULT NULL,
  `menu_link` varchar(100) DEFAULT NULL,
  `menu_sequent` int(11) DEFAULT '1',
  `menu_key` varchar(32) DEFAULT NULL,
  `is_label` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`submenu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rwds_system_submenu
-- ----------------------------
INSERT INTO `rwds_system_submenu` VALUES ('1', '2', 'ผู้ใช้งานระบบ', 'icon-user-md', 'user_admin/index', '1', '0914413975b88c951f08f2dc073a5079', 'no');
INSERT INTO `rwds_system_submenu` VALUES ('2', '2', 'กลุ่มผู้ใช้งาน', 'icon-group', 'user_group/index', '2', '5e975624c5a4e4cb9fddde6da3bda19f', 'no');
INSERT INTO `rwds_system_submenu` VALUES ('3', '2', 'เมนูระบบหลังบ้าน', 'icon-bars', 'menu_manager/index', '3', 'e5d42c8b4fde26dd387ea108252d8e4c', 'no');
INSERT INTO `rwds_system_submenu` (`submenu_id`, `menu_id`, `menu_label`, `menu_icon`, `menu_link`, `menu_sequent`, `menu_key`, `is_label`) VALUES (4, 2, 'การจัดการข้อมูลบริษัท', 'icon-building', 'managecompany/index', 1, '901b5dbc1f07beb25b9eab9798f3adec', 'no');

-- ----------------------------
-- Table structure for rwds_system_url_rewrite
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_url_rewrite`;
CREATE TABLE `rwds_system_url_rewrite` (
  `url_rewrite_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_path` varchar(255) DEFAULT NULL,
  `target_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`url_rewrite_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rwds_system_url_rewrite
-- ----------------------------

-- ----------------------------
-- Table structure for rwds_system_usergroup_permision
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_usergroup_permision`;
CREATE TABLE `rwds_system_usergroup_permision` (
  `perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `perm_group_id` int(11) NOT NULL,
  `perm_menu_key` varchar(32) NOT NULL,
  `perm_read` enum('true','false') DEFAULT 'true',
  `perm_write` enum('true','false') DEFAULT 'false',
  `perm_delete` enum('true','false') DEFAULT 'false',
  `perm_status` enum('active','disactive','delete') DEFAULT 'active',
  PRIMARY KEY (`perm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of rwds_system_usergroup_permision
-- ----------------------------
INSERT INTO `rwds_system_usergroup_permision` VALUES ('1', '2', 'edfd09359a11bb7f7448016a7b43b6d6', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('2', '2', '80033f110ee53904880d805a01cf465d', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('3', '2', 'd5420e0b51703448e03945cc153b46d5', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('4', '2', '44d3996b757bd97f02a45f301e4d3eb6', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('5', '2', '9aecaf6bc39baf0bed05075bb2ff76a3', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('6', '2', '7e81a8feb211f1f91dfedd980081b0c8', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('7', '2', 'da3c0bfe26fea8e3d49372b5e7dbc3d5', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('8', '2', '9364003093052248985df62e67c4de0e', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('9', '2', 'b831224f2822e6b23f877005d8b227ef', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('10', '2', 'fee3681c4075df13c7e1a1789819ea95', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('11', '2', '5e975624c5a4e4cb9fddde6da3bda19f', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('12', '2', 'ec4d54db7cf0852b2dd6b97e0729f4e5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('13', '2', '36b27a92d944126c4f991c087c2034ad', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('14', '2', 'da40adcacd3a989d086c0d6ca7acc682', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('15', '2', 'a03575159c34ee6787bd96acdab35e6a', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('16', '2', '862f4fdc8960f5b734ed5a406a77c26f', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('17', '2', 'bb3d4b711f6d0f1eb0203c0a22deae6e', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('18', '2', '3de4fff4e1c8836c358f714aac979df6', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('19', '2', '7e6b8e969214eb84ca25ce7c877da5d6', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('20', '2', '772026180ded3355bca03358dea9d7d7', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('21', '2', '2600a01d4c9a9eb483b8436f7e5f0392', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('22', '2', '1ef6e009e027b456107afde53cd181c3', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('23', '2', '14bfc93cb9652aaf196326fb2d27b0c6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('24', '2', '47a77dd1b8e5098d37d85ac4ea8281d9', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('25', '2', 'bd485667947a4cd0542e70042e23347a', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('26', '1', 'edfd09359a11bb7f7448016a7b43b6d6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('27', '1', '80033f110ee53904880d805a01cf465d', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('28', '1', 'd5420e0b51703448e03945cc153b46d5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('29', '1', '44d3996b757bd97f02a45f301e4d3eb6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('30', '1', '9aecaf6bc39baf0bed05075bb2ff76a3', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('31', '1', '7e81a8feb211f1f91dfedd980081b0c8', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('32', '1', 'da3c0bfe26fea8e3d49372b5e7dbc3d5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('33', '1', '36b27a92d944126c4f991c087c2034ad', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('34', '1', 'da40adcacd3a989d086c0d6ca7acc682', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('35', '1', '9364003093052248985df62e67c4de0e', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('36', '1', 'b831224f2822e6b23f877005d8b227ef', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('37', '1', 'fee3681c4075df13c7e1a1789819ea95', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('38', '1', 'a03575159c34ee6787bd96acdab35e6a', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('39', '1', '862f4fdc8960f5b734ed5a406a77c26f', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('40', '1', 'bb3d4b711f6d0f1eb0203c0a22deae6e', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('41', '1', '3de4fff4e1c8836c358f714aac979df6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('42', '1', '7e6b8e969214eb84ca25ce7c877da5d6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('43', '1', '772026180ded3355bca03358dea9d7d7', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('44', '1', '2600a01d4c9a9eb483b8436f7e5f0392', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('45', '1', '5e975624c5a4e4cb9fddde6da3bda19f', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('46', '1', 'ec4d54db7cf0852b2dd6b97e0729f4e5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('47', '1', '1ef6e009e027b456107afde53cd181c3', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('48', '1', '14bfc93cb9652aaf196326fb2d27b0c6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('49', '1', '47a77dd1b8e5098d37d85ac4ea8281d9', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('50', '1', 'bd485667947a4cd0542e70042e23347a', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('51', '3', 'edfd09359a11bb7f7448016a7b43b6d6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('52', '3', '80033f110ee53904880d805a01cf465d', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('53', '3', 'd5420e0b51703448e03945cc153b46d5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('54', '3', '44d3996b757bd97f02a45f301e4d3eb6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('55', '3', '9aecaf6bc39baf0bed05075bb2ff76a3', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('56', '3', '7e81a8feb211f1f91dfedd980081b0c8', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('57', '3', 'da3c0bfe26fea8e3d49372b5e7dbc3d5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('58', '3', '36b27a92d944126c4f991c087c2034ad', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('59', '3', 'da40adcacd3a989d086c0d6ca7acc682', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('60', '3', '9364003093052248985df62e67c4de0e', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('61', '3', 'b831224f2822e6b23f877005d8b227ef', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('62', '3', 'fee3681c4075df13c7e1a1789819ea95', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('63', '3', 'a03575159c34ee6787bd96acdab35e6a', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('64', '3', '862f4fdc8960f5b734ed5a406a77c26f', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('65', '3', 'bb3d4b711f6d0f1eb0203c0a22deae6e', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('66', '3', '3de4fff4e1c8836c358f714aac979df6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('67', '3', '7e6b8e969214eb84ca25ce7c877da5d6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('68', '3', '772026180ded3355bca03358dea9d7d7', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('69', '3', '2600a01d4c9a9eb483b8436f7e5f0392', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('70', '3', '5e975624c5a4e4cb9fddde6da3bda19f', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('71', '3', 'ec4d54db7cf0852b2dd6b97e0729f4e5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('72', '3', '1ef6e009e027b456107afde53cd181c3', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('73', '3', '14bfc93cb9652aaf196326fb2d27b0c6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('74', '3', '47a77dd1b8e5098d37d85ac4ea8281d9', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('75', '3', 'bd485667947a4cd0542e70042e23347a', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('76', '2', '0914413975b88c951f08f2dc073a5079', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('77', '4', 'edfd09359a11bb7f7448016a7b43b6d6', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('78', '4', '80033f110ee53904880d805a01cf465d', 'true', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('79', '4', 'd5420e0b51703448e03945cc153b46d5', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('80', '4', '44d3996b757bd97f02a45f301e4d3eb6', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('81', '4', '9aecaf6bc39baf0bed05075bb2ff76a3', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('82', '4', '7e81a8feb211f1f91dfedd980081b0c8', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('83', '4', 'da3c0bfe26fea8e3d49372b5e7dbc3d5', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('84', '4', '36b27a92d944126c4f991c087c2034ad', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('85', '4', 'da40adcacd3a989d086c0d6ca7acc682', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('86', '4', '9364003093052248985df62e67c4de0e', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('87', '4', 'b831224f2822e6b23f877005d8b227ef', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('88', '4', 'fee3681c4075df13c7e1a1789819ea95', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('89', '4', 'a03575159c34ee6787bd96acdab35e6a', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('90', '4', '862f4fdc8960f5b734ed5a406a77c26f', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('91', '4', 'bb3d4b711f6d0f1eb0203c0a22deae6e', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('92', '4', '3de4fff4e1c8836c358f714aac979df6', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('93', '4', '7e6b8e969214eb84ca25ce7c877da5d6', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('94', '4', '772026180ded3355bca03358dea9d7d7', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('95', '4', '2600a01d4c9a9eb483b8436f7e5f0392', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('96', '4', '5e975624c5a4e4cb9fddde6da3bda19f', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('97', '4', '0914413975b88c951f08f2dc073a5079', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('98', '4', 'ec4d54db7cf0852b2dd6b97e0729f4e5', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('99', '4', '1ef6e009e027b456107afde53cd181c3', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('100', '4', '14bfc93cb9652aaf196326fb2d27b0c6', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('101', '4', '47a77dd1b8e5098d37d85ac4ea8281d9', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('102', '4', 'bd485667947a4cd0542e70042e23347a', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('103', '5', 'edfd09359a11bb7f7448016a7b43b6d6', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('104', '5', 'fee3681c4075df13c7e1a1789819ea95', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('105', '5', 'a03575159c34ee6787bd96acdab35e6a', 'true', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('106', '5', '862f4fdc8960f5b734ed5a406a77c26f', 'true', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('107', '5', '772026180ded3355bca03358dea9d7d7', 'true', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('108', '5', '7e81a8feb211f1f91dfedd980081b0c8', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('109', '5', 'da3c0bfe26fea8e3d49372b5e7dbc3d5', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('110', '5', '80033f110ee53904880d805a01cf465d', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('111', '5', '44d3996b757bd97f02a45f301e4d3eb6', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('112', '5', '5e975624c5a4e4cb9fddde6da3bda19f', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('113', '5', '0914413975b88c951f08f2dc073a5079', 'false', 'false', 'false', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('114', '4', '6f4922f45568161a8cdf4ad2299f6d23', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('115', '4', '6f4922f45568161a8cdf4ad2299f6dss', 'false', 'false', 'false', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('116', '4', '6851056058e9a66485c79c9dce35afae', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('117', '4', '1b51afd8349bf0bde2c50e41c976e8e6', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('118', '4', '7098e8f7a528f7cae0137f15430bb55a', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('119', '4', '4ea1beeb74c63c31f1e65a66604906a3', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('120', '4', '2b25510b863db3860df3171a72e6bd61', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('121', '4', 'd64d36e05aab3ab779b9d9a209b63369', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('122', '4', 'acb5e451f81f68703aeb70c406e3f14d', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('123', '4', 'a7b1faf37d2f1a6f08b3de5ce7d1dbcd', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('124', '4', '9b59e452e332fb27879cc0762fab0a8a', 'true', 'true', 'true', 'delete');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('125', '2', 'd41d8cd98f00b204e9800998ecf8427e', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('126', '2', 'a206afbf99e3cc0ab90403e186c635e1', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('127', '2', '6b179b4b922e0a2da76762d79f761ff5', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('128', '2', '271fee0a6f4ff414ba77fa5ba74db5bf', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('129', '2', '0173b607ea68c4af5008a0942bb7e951', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('130', '2', '61e7b859f2b051604b77401be1ed3ffc', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('131', '2', '4b7caa022eefa2b630763fd0f05ec4d1', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('132', '2', '0b9c56027f5f70a52246f612084e294c', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('133', '2', 'ab6dce64cc08c66c7bf1b83acd4215f9', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('134', '2', '815339ccaea8fdfbb1a2cdd242c19f9c', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('135', '2', 'a8ff0b967d8e9cda6122183af245d1ae', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('136', '2', '1bdb9da104e5796f401cca761ef1c9f2', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('137', '2', '833675c0ae6c061f4e8d87c294ebb1ec', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('138', '2', '93c731f1c3a84ef05cd54d044c379eaa', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('139', '2', 'dc21d8fa23801035cb9eb8da98343d92', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('140', '2', '6bda158193aaec74f6409a7f315b3e09', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('141', '2', 'e5d42c8b4fde26dd387ea108252d8e4c', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('142', '2', 'f5110dd9cda85dc46eda63e3ebafbc7c', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('143', '2', '642713d7d2b972e6601577f253e6057b', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('144', '2', '8a11fc1831fdfa2b49e8203ff134f4ec', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('145', '2', '0167c858306624390a0c7ac5041aadb1', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('146', '2', 'ce01da7485b1b183e6b32a08d4d64f47', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('147', '2', 'eb0f48a107df1a0f343d4cd513b555e6', 'true', 'true', 'true', 'active');
INSERT INTO `rwds_system_usergroup_permision` VALUES ('148', '2', 'c17761a8879743cfb9a2219e2d7b0199', 'true', 'true', 'true', 'active');

-- ----------------------------
-- Table structure for rwds_system_users
-- ----------------------------
DROP TABLE IF EXISTS `rwds_system_users`;
CREATE TABLE `rwds_system_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `user_fullname` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_mobileno` char(10) DEFAULT NULL,
  `user_group` int(11) DEFAULT NULL,
  `user_avatar` varchar(50) DEFAULT NULL,
  `user_joinby` int(11) DEFAULT NULL,
  `user_joindate` datetime DEFAULT NULL,
  `user_joinip` varchar(15) DEFAULT NULL,
  `user_updatedate` datetime DEFAULT NULL,
  `user_updateby` int(11) DEFAULT NULL,
  `user_updateip` datetime DEFAULT NULL,
  `user_status` enum('active','suspend','delete') DEFAULT 'suspend',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rwds_system_users
-- ----------------------------
/*
INSERT INTO `rwds_system_users` VALUES ('1', 'mycools', 'b8659ab35f551cdd2ef4332a1471d5de', 'Jarak Kritkiattisak', 'admin@mycools.in.th', '0896561688', '1', '3c969a6b07c0b173126b62a77149a693.png', '1', '2013-01-21 16:41:07', '203.151.20.171', '2014-07-05 18:57:49', '1', '2019-02-16 08:40:18', 'active');
INSERT INTO `rwds_system_users` VALUES ('2', 'iaon', 'd25275d8a0ee5fbcbdc223db9fa2c394', 'Pubes (Aon) Palarak', 'pubes@winter.co.th', '0811111111', '1', null, '1', '2013-03-12 11:53:14', '58.10.65.223', '2014-06-24 01:38:08', '1', '2014-06-24 01:38:14', 'active');
INSERT INTO `rwds_system_users` VALUES ('4', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin', 'pubes@winter.co.th', '0855555555', '1', null, '1', '2013-08-13 15:05:34', '110.77.229.196', '2014-08-30 10:12:45', '2', '2017-02-21 17:02:00', 'active');
INSERT INTO `rwds_system_users` VALUES ('5', 'darkneoz', '811e0cf93631f4ee1101c6222f545ce4', 'Arnut Jiamrattanapitak', 'tle417@gmail.com', '0841943525', '1', null, '1', '2014-06-24 01:38:01', '110.77.229.196', '2013-12-03 11:11:03', '1', '2014-06-24 01:38:54', 'active');
*/
INSERT INTO `rwds_system_users` VALUES ('1', 'ikidz', '89070af207057626301d6f3a78efc207', 'Supawee Ippoodom', 'i.supawee@gmail.com', '0814804009', '1', null, '1', '2013-12-02 02:19:24', '171.6.163.161', '2014-07-22 22:50:49', '10', '2019-02-16 08:40:16', 'active');
-- INSERT INTO `rwds_system_users` VALUES ('11', 'piyathida', '44626b04b2d9c0eba81d21943e91413c', 'piyathida panapitakkul', 'piyathida_pa@hotmail.com', '0855744525', '2', null, '4', '2014-04-23 22:50:32', '27.130.77.238', '2014-04-23 22:50:32', '4', '2014-06-24 01:38:58', 'active');
