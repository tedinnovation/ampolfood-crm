alter table `rwds_pts_logs` add `user_id` bigInt not null default 0 after `reference_id`;
alter table `rwds_pts_logs` add `total_point` int(11) not null default 0 after `logs_point`;