<?php /* Section "#body" - Start */ ?>
<section id="body" class="order-index">
    <div class="container d-flex flex-wrap">

        <?php /* Content - Start */ ?>
        <div class="col-12 box box-shadow">
            <div class="col-12 col-sm-10 offset-sm-1 nopadding-xs">
                <h3 class="c-green text-xs-center text-sm-left">ตะกร้าของรางวัล</h3>
                <div class="table-responsive">
                    <table class="table table-striped bg-white">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center">ลำดับที่</th>
                                <th class="text-center">รูปสินค้า</th>
                                <th class="text-center">ชื่อของรางวัล</th>
                                <th class="text-center">จำนวน</th>
                                <th class="text-center">คะแนนแลกรับสิทธิ์</th>
                                <th class="text-center">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for( $i=1; $i<=5; $i++ ): ?>
                                <tr>
                                    <td class="align-middle text-center"><?php echo $i; ?></td>
                                    <td class="align-middle justify-content-center d-flex align-items-center">
                                        <img src="img/mockup_product.png" alt="" width="120" class="img-fullwidth custom-border custom-border-2px custom-border-green" />
                                    </td>
                                    <td class="align-middle text-center">Product <?php echo $i; ?></td>
                                    <td class="align-middle text-center">1 ชิ้น</td>
                                    <td class="align-middle text-center"><?php echo number_format(20); ?> คะแนน</td>
                                    <td class="align-middle text-center">
                                        <a href="javascript:void(0);" class="btn btn-green">ลบ</a>
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        </tbody>
                        <tfoot class="bg-green">
                            <tr>
                                <td colspan="5" class="text-right c-white">รวมคะแนนแลกรับสิทธิ์</td>
                                <td class="text-center c-white"><?php echo number_format(20); ?> คะแนน</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right c-white">คะแนนคงเหลือ</td>
                                <td class="text-center c-white"><?php echo number_format(20); ?> คะแนน</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <p class="d-flex d-md-none align-items-center justify-content-center"><i class="fas fa-arrows-alt-h mr-3 c-green"></i> Scroll to see more information.</p>
                <p>&nbsp;</p>
                <div id="control-button" class="d-flex justify-content-between">
                    <a href="<?php echo site_url('product'); ?>" class="btn btn-green">เลือกของรางวัล</a>
                    <a href="<?php echo site_url('order/summary'); ?>" class="btn btn-green">ยืนยัน</a>
                </div>
            </div>
        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>