<?php /* Section "#body" - Start */ ?>
<section id="body" class="order-thankyou">
    <div class="container d-flex">

        <?php /* Content - Start */ ?>
        <div class="col-12 box box-shadow d-flex justify-content-center">
            <div class="wrapper mt-auto mb-auto">
                <h5 class="text-center c-brown">ขอบคุณสำหรับการแลกของรางวัลกับ Good Life For You</h5>
                <h5 class="text-center c-brown">รายการของรางวัลจะถูกส่งไปยังอีเมลของคุณ</h5>
                <p>&nbsp;</p>
                <p class="text-center">
                    <a href="<?php echo site_url('product'); ?>" class="btn btn-green">กลับหน้าแรก</a>
                </p>
            </div>
        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>