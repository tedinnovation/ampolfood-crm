<?php /* Section "#body" - Start */ ?>
<section id="body" class="order-summary">
    <div class="container d-flex flex-wrap">

        <?php /* Content - Start */ ?>
        <div class="col-12 box box-shadow">
            <div class="col-12 col-sm-10 offset-sm-1 nopadding-xs d-flex flex-wrap">
                <h3 class="col-12 c-green text-xs-center text-sm-left">สรุปรายการแลกของรางวัล</h3>
                <div class="table-responsive col-12 nopadding">
                    <table class="table table-striped bg-white">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center">ลำดับที่</th>
                                <th class="text-center">รูปสินค้า</th>
                                <th class="text-center">ชื่อของรางวัล</th>
                                <th class="text-center">จำนวน</th>
                                <th class="text-center">คะแนนแลกรับสิทธิ์</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for( $i=1; $i<=5; $i++ ): ?>
                                <tr>
                                    <td class="align-middle text-center"><?php echo $i; ?></td>
                                    <td class="align-middle justify-content-center d-flex align-items-center">
                                        <img src="img/mockup_product.png" alt="" width="120" class="img-fullwidth custom-border custom-border-2px custom-border-green" />
                                    </td>
                                    <td class="align-middle text-center">Product <?php echo $i; ?></td>
                                    <td class="align-middle text-center">1 ชิ้น</td>
                                    <td class="align-middle text-center"><?php echo number_format(20); ?> คะแนน</td>
                                </tr>
                            <?php endfor; ?>
                        </tbody>
                        <tfoot class="bg-green">
                            <tr>
                                <td colspan="4" class="text-right c-white">รวมคะแนนแลกรับสิทธิ์</td>
                                <td class="text-center c-white"><?php echo number_format(20); ?> คะแนน</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="text-right c-white">คะแนนคงเหลือ</td>
                                <td class="text-center c-white"><?php echo number_format(20); ?> คะแนน</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <p class="col-12 d-flex d-md-none align-items-center justify-content-center"><i class="fas fa-arrows-alt-h mr-3 c-green"></i> Scroll to see more information.</p>
                <div class="col-12 order-2 order-md-1 nopadding">
                    <p>&nbsp;</p>
                    <div id="control-button" class="d-flex justify-content-end">
                        <a href="<?php echo site_url('order'); ?>" class="btn btn-green">ย้อนกลับ</a>
                        <a href="<?php echo site_url('order/confirm'); ?>" class="btn btn-green">ยืนยัน</a>
                    </div>
                </div>
                <div class="col-12 order-1 order-md-2 nopadding">
                    <p>&nbsp;</p>
                    <p>ข้อมูลสมาชิก</p>
                    <p>ชื่อ - สกุล : User Name</p>
                    <p>อีเมล : email@domain.com</p>
                    <p>เบอร์โทรศัพท์ : 0616250553</p>
                    <p>ที่อยู่จัดส่ง :</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sollicitudin elit nisi, nec dapibus metus blandit in.</p>
                </div>
            </div>
        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>