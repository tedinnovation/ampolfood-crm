<?php /* Section "#body" - Start */ ?>
<section id="body" class="member-points">
    <div class="container d-flex flex-wrap no-background">

        <?php /* Sidebar - Start */ ?>
        <?php $this->load->view('member/sidebar'); ?>
        <?php /* Sidebar - End */ ?>

        <?php /* Content - Start */ ?>
        <div class="col-12 col-sm-9 box box-shadow">

            <div class="col-12 offset-0 col-sm-10 offset-sm-1 nopadding-xs">
                <h3 class="c-green">วันที่แลกของรางวัล <span class="c-brown"><?php echo date( 'd - m - Y H : i', strtotime( '2018-11-09 20:23' ) ); ?></span></h3>
                <h3><small>หมายเลขคำสั่งซื้อ</small> xxx-xxx-xxxx</h3>
                <h5>รายการสั่งซื้อสินค้า</h5>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <?php for( $i=1; $i<=2; $i++ ): ?>
                                <tr>
                                    <td><?php echo $i; ?>. ชุดสุขภาพครอบครัวสุดฮอต</td>
                                    <td class="text-center">จำนวน 1 ชิ้น</td>
                                    <td class="text-right pr-3">มูลค่า <?php echo number_format(515); ?> บาท</td>
                                </tr>
                            <?php endfor; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" class="text-right">ยอดรวม</td>
                                <td class="text-right pr-3"><?php echo number_format(1030); ?> บาท</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">ค่าจัดส่ง</td>
                                <td class="text-right pr-3"><?php echo number_format(0); ?> บาท</td>
                            </tr>
                            <tr class="bg-green">
                                <td colspan="2" class="pl-3 c-white text-right">ส่วนลด</td>
                                <td class="c-white text-right pr-3"><?php echo number_format(20); ?> บาท</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">ยอดรวมสุทธิ</td>
                                <td class="text-right pr-3"><?php echo number_format(1030); ?> บาท</td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">คะแนนที่ใช้แลกของรางวัล</td>
                                <td class="text-right pr-3"><?php echo number_format(20); ?> คะแนน</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <p class="d-flex d-md-none align-items-center justify-content-center"><i class="fas fa-arrows-alt-h mr-3 c-green"></i> Scroll to see more information.</p>
                <p>ที่อยู่จัดส่ง</p>
                <p>คุณ xxxxxx  xxxxx</p>
                <p>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>
                <p>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>
                <p>&nbsp;</p>
                <p class="text-center">
                    <a href="<?php echo site_url('member/history'); ?>" class="btn btn-green">ย้อนกลับ</a>
                </p>
            </div>
        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>