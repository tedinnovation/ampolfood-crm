<?php /* Section "#body" - Start */ ?>
<section id="body" class="member-profile">
    <div class="container d-flex flex-wrap no-background">

        <?php /* Sidebar - Start */ ?>
        <?php $this->load->view('member/sidebar'); ?>
        <?php /* Sidebar - End */ ?>

        <?php /* Content - Start */ ?>
        <div class="col-12 col-sm-9 box box-shadow">
            
            <div class="col-12 offset-0 col-sm-10 offset-sm-1 nopadding-xs">
                <h3 class="c-green text-xs-center text-sm-left">ข้อมูลของฉัน</h3>
                <form id="profileForm" name="profileForm" method="post" enctype="multipart/form-data" action="" class="form">
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">หมายเลขสมาชิก</label>
                        <p class="col-6 col-sm-8 nopadding">123-4567-890</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-12 col-sm-4 mb-0 nopadding-xs" for="user_email">อีเมล</label>
                        <input type="text" name="user_email" id="user_email" value="email@domain.com" disabled class="col-10 col-sm-6 nopadding-xs" />
                        <p class="col-2 text-center"><a href="javascript:void(0);" class="underline">แก้ไข</a></p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-12 col-sm-4 mb-0 nopadding-xs" for="user_password">รหัสผ่าน</label>
                        <input type="password" name="user_password" id="user_password" value="" disabled class="col-10 col-sm-6 nopadding-xs" />
                        <p class="col-2 text-center"><a href="javascript:void(0);" class="underline">แก้ไข</a></p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">ชื่อสมาชิก</label>
                        <p class="col-6 col-sm-8 nopadding">UserAPF01</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">ชื่อ - นามสกุล</label>
                        <p class="col-6 col-sm-8 nopadding">คุณ อำพล ฟูดส์รีเทล</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">เพศ</label>
                        <p class="col-6 col-sm-8 nopadding">ชาย</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">ที่อยู่</label>
                        <p class="col-6 col-sm-8 nopadding">เลขที่ 57 หมู่ 3 ต.กระทุ่มล้ม อ.สามพราน จ.นครปฐม</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">เบอร์โทรศัพท์</label>
                        <p class="col-6 col-sm-8 nopadding">062238538</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">LINE ID</label>
                        <p class="col-6 col-sm-8 nopadding">@APZAAA</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">วัน/เดือน/ปีเกิด</label>
                        <p class="col-6 col-sm-8 nopadding">1 / 2 / 1987</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <label class="control-label col-6 col-sm-4 mb-0 nopadding-xs">หมายเลขบัตรประชาชน</label>
                        <p class="col-6 col-sm-8 nopadding">1123525895587</p>
                    </div>
                    <div class="control-group d-flex flex-wrap align-items-center">
                        <p class="col-12 nopadding-xs">*หากต้องการเปลี่ยนแปลงข้อมูล กรุณาเข้าไปที่ <a href="https://www.goodlifeforyou.com" target="_blank" class="c-green">www.goodlifeforyou.com</a></p>
                    </div>
                </form>
            </div>

        </div>
        <?php /* Content - End */ ?>

    </div>
</section>