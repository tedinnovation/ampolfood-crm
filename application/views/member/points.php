<?php /* Section "#body" - Start */ ?>
<section id="body" class="member-points">
    <div class="container d-flex flex-wrap no-background">

        <?php /* Sidebar - Start */ ?>
        <?php $this->load->view('member/sidebar'); ?>
        <?php /* Sidebar - End */ ?>

        <?php /* Content - Start */ ?>
        <div class="col-12 col-sm-9 box box-shadow">

            <div class="col-12 offset-0 col-sm-10 offset-sm-1 nopadding-xs">
                <h3 class="c-green text-xs-center text-sm-left">คะแนนที่สามารถใช้ได้ : <small><?php echo number_format(2000); ?> คะแนน</small></h3>
                <form id="pointFilterForm" name="pointFilterForm" method="post" enctype="multipart/form-data" action="" class="form">
                    <div class="control-group d-flex flex-wrap justify-content-between align-items-center">
                        <label class="control-label col-12 col-sm-3 nopadding text-xs-center text-sm-left" for="sortDate">ค้นหาตั้งแต่วันที่</label>
                        <div class="input-wrapper col-12 col-sm-6 nopadding">
                            <input type="text" name="sortDate" id="sortDate" value="" class="datepicker custom-border custom-border-2px custom-border-lightgrey" />
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="col-12 col-sm-3 nopadding text-xs-center text-sm-right">
                            <button type="submit" name="btn-sort" class="btn btn-green">ค้นหา</button>
                        </div>
                    </div>
                </form>
                <div class="col-12 nopadding table-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-center">ลำดับที่</th>
                                    <th class="text-center">วันที่และเวลา</th>
                                    <th class="text-center">คะแนน</th>
                                    <th class="text-center">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for( $i=1; $i<=5; $i++ ): ?>
                                    <tr>
                                        <td class="align-middle text-center"><?php echo $i; ?></td>
                                        <td class="align-middle text-center"><?php echo date( 'd - m - Y H : i', strtotime( '2018-11-01 20:23:00' ) ); ?></td>
                                        <td class="align-middle text-center">+ 20</td>
                                        <td class="align-middle text-center">
                                            <a href="<?php echo site_url('member/pointinfo/'.$i); ?>" class="btn btn-green">ดูรายละเอียด</a>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                    <p class="d-flex d-md-none align-items-center justify-content-center"><i class="fas fa-arrows-alt-h mr-3 c-green"></i> Scroll to see more information.</p>
                </div>

                <?php /* Pagination - Start */ ?>
                <div class="pagination">
                    <ul class="d-flex justify-content-center ml-auto mr-auto align-items-center">
                        <li class="ml-3 mr-3"><a href="javascript:void(0);" class="btn btn-green">ก่อนหน้า</a></li>
                        <?php for( $i=1; $i<=5; $i++ ): ?>
                            <li class="number ml-3 mr-3 <?php echo ( $i == 1 ? 'active' : '' ); ?>"><a href="javascript:void(0);"><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <li class="ml-3 mr-3"><a href="javascript:void(0);" class="btn btn-green">ถัดไป</a></li>
                    </ul>
                </div>
                <?php /* Pagination - End */ ?>
            </div>

        </div>
        <?php /* Content - End */ ?>

    </div>
</div>
<?php /* Section "#body" - End */ ?>