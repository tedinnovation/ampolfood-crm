<?php /* For Desktop Version - Start */ ?>
<div id="sidebar" class="col-3 d-none d-sm-block pl-0">
    <div class="wrapper col-12">
        <h3 class="c-green text-center">ชื่อสมาชิก</h3>
        <ul>
            <li><a href="<?php echo site_url('member'); ?>" class="c-white text-center">บัญชีของฉัน</a></li>
            <li><a href="<?php echo site_url('member/points'); ?>" class="c-white text-center">ตรวจสอบคะแนน</a></li>
            <li><a href="<?php echo site_url('member/history'); ?>" class="c-white text-center">ประวัติการแลกของรางวัล</a></li>
            <li><a href="<?php echo site_url('auth/signout'); ?>" class="c-white text-center">ออกจากระบบ</a></li>
        </ul>
    </div>
</div>
<?php /* For Desktop Version - End */ ?>

<?php /* For Mobile Version - Start */ ?>
<div id="mobile-sidebar" class="col-12 d-flex d-sm-none flex-wrap">
    <p class="c-green text-center"><i class="fa fa-user c-green"></i> ชื่อสมาชิก <i class="fa fa-chevron-down c-green"></i></p>
    <ul class="bg-green">
        <li><a href="<?php echo site_url('member'); ?>" class="c-white text-center">บัญชีของฉัน</a></li>
        <li><a href="<?php echo site_url('member/points'); ?>" class="c-white text-center">ตรวจสอบคะแนน</a></li>
        <li><a href="<?php echo site_url('member/history'); ?>" class="c-white text-center">ประวัติการแลกของรางวัล</a></li>
        <li><a href="<?php echo site_url('auth/signout'); ?>" class="c-white text-center">ออกจากระบบ</a></li>
    </ul>
</div>
<?php /* For Mobile Version - End */ ?>