<?php /* Section "#body" - Start */ ?>
<section id="body" class="contact-member">
    <div class="container d-flex">

        <?php /* Content - Start */ ?>
        <div class="col-12 box box-shadow">

            <div class="box-header d-flex justify-content-center align-items-center col-12">
                <h3 class="c-green mr-auto ml-auto leaf-left">APF Family Card</h3>
            </div>

            <p class="text-center"><img src="img/member_card.jpg" alt="" class="img-fullwidth" /></p>

        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>