<?php /* Section "#body" - Start */ ?>
<section id="body" class="contact-index">
    <div class="container d-flex">

        <?php /* Content - Start */ ?>
        <div class="col-12 box d-flex flex-wrap no-background">

            <div class="box-header d-flex justify-content-center align-items-center col-12">
                <h3 class="c-green mr-auto ml-auto leaf-left">ติดต่อเรา</h3>
            </div>

            <div class="wrapper col-12 offset-0 col-sm-10 offset-sm-1 nopadding-xs">

                <?php /* Box Item - Start */ ?>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_1.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h4 class="c-green">สำนักงานใหญ่ ท่าเตียน</h4>
                            <p class="c-brown">392/56-57 ซอยปรีชาพาณิชย์ ถนนมหาราช</p>
                            <p class="c-brown">แขวงพระบรมมหาราชวัง เขตพระนคร กรุงเทพฯ 10200</p>
                            <p class="c-brown">โทรศัพท์ส่วนกลาง: +66(0) 2622-3434 (auto)</p>
                            <p class="c-brown">โทรสาร: +66(0) 2226-1829</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_2.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h4 class="c-green">สำนักงานปิ่นเกล้า</h4>
                            <p class="c-brown">5/895 ชั้น 2 ถนนบรมราชชนนี แขวงอรุณอัมรินทร์</p>
                            <p class="c-brown">เขตบางกอกน้อย กรุงเทพฯ 10700</p>
                            <p class="c-brown">โทรศัพท์: +66(0) 2622-3434 (auto)</p>
                            <p class="c-brown">โทรสาร: +66(0) 2226-1829</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_3.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h4 class="c-green">โรงงาน พุทธมณฑล สาย 5</h4>
                            <p class="c-brown">57 หมู่ 3 ตำบลกระทุ่มล้ม อำเภอสามพราน</p>
                            <p class="c-brown">จังหวัดนครปฐม 73220</p>
                            <p class="c-brown">โทรศัพท์ : +66(0) 2497-9111</p>
                            <p class="c-brown">โทรสาร : +66(0) 2420-4781</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_4.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h4 class="c-green">โรงงานราชบุรี</h4>
                            <p class="c-brown">ต.ดอนกระเบื้อง อ.บ้านโป่ง</p>
                            <p class="c-brown">จ.ราชบุรี</p>
                            <p class="c-brown">โทรศัพท์ : อยู่ระหว่างดำเนินการ</p>
                            <p class="c-brown">โทรสาร : อยู่ระหว่างดำเนินการ</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_5.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h5 class="c-green">ศูนย์นวัตกรรมอำพลฟูดส์(APF Innovation Center)</h5>
                            <p class="c-brown">57 หมู่ 3 ตำบลกระทุ่มล้ม อำเภอสามพราน</p>
                            <p class="c-brown">จังหวัดนครปฐม 73220</p>
                            <p class="c-brown">โทรศัพท์ : +66(0) 2811-8550-3</p>
                            <p class="c-brown">โทรสาร : +66(0) 2420-4781</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <?php /* Box Item - End */ ?>

            </div>

            <div class="box-header d-flex justify-content-center align-items-center col-12">
                <h3 class="c-green mr-auto ml-auto leaf-left">ศูนย์กระจายสินค้า</h3>
            </div>

            <div class="wrapper col-12 offset-0 col-sm-10 offset-sm-1 nopadding-xs">

                <?php /* Box Item - Start */ ?>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_6.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h4 class="c-green">ศูนย์กระจายสินค้าดอนตูม</h4>
                            <p class="c-brown">103/1 หมู่ 7 ต.สามง่าม อ.ดอนตูม</p>
                            <p class="c-brown">จ.นครปฐม 73150</p>
                            <p class="c-brown">โทรศัพท์ : +66(0) 3430-0131</p>
                            <p class="c-brown">โทรสาร : -</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <div class="items d-flex flex-wrap box-shadow bg-white">
                    <div class="col-12 col-sm-6 nopadding">
                        <img src="img/contact_7.jpg" alt="" class="img-maxwidth" />
                    </div>
                    <div class="col-12 col-sm-6 nopadding d-flex align-items-center">
                        <div class="wrapper-info w-100 text-center">
                            <h4 class="c-green">ศูนย์กระจายสินค้า จ.ขอนแก่น</h4>
                            <p class="c-brown">67 หมู่ 8 ถนนเหล่านาดี ต.เมืองเก่า อ.เมืองขอนแก่น</p>
                            <p class="c-brown">จ.ขอนแก่น 40000</p>
                            <p class="c-brown">โทรศัพท์ : +66(0) 4300-2230</p>
                            <p class="c-brown">โทรสาร : -</p>
                            <p><a href="javascript:void(0);" class="btn btn-green">ดาวน์โหลดแผนที่</a></p>
                        </div>
                    </div>
                </div>
                <?php /* Box Item - End */ ?>

            </div>

        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>