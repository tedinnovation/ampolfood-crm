<?php /* Section "#body" - Start */ ?>
<section id="body" class="member-index">
    <div class="container box-shadow">

        <form id="signinForm" name="signinForm" method="post" enctype="multipart/form-data" class="form col-12 col-sm-6 ml-auto mr-auto">

            <div class="box-header d-flex justify-content-center">
                <h3 class="c-green leaf-left">เข้าสู่ระบบสมาชิก</h3>
            </div>

            <div class="control-group">
                <input type="text" name="signin_username" id="signin_username" value="<?php echo set_value('signin_username'); ?>" placeholder="ชื่อผู้ใช้งาน" class="border border-1px border-lightgrey" />
            </div>
            <?php echo form_error('signin_username'); ?>
            <div class="control-group message-wrapper">
                <p class="message error">TEST ERROR!</p>
            </div>

            <div class="control-group">
                <input type="password" name="signin_password" id="signin_password" value="<?php echo set_value('signin_password'); ?>" placeholder="รหัสผ่าน" class="border border-1px border-lightgrey" />
            </div>
            <?php echo form_error('signin_password'); ?>

            <div class="control-group">
                <p class="text-right">
                    <a href="javascript:void(0);" class="c-grey">ลืมรหัสผ่าน</a>
                </p>
            </div>

            <div class="control-group d-flex justify-content-between col-12 offset-0 col-sm-6 offset-sm-3">
                <button type="button" name="btn-signup" id="btn-signup" class="btn btn-green col-5 px-0">สมัครสมาชิก</button>
                <button type="button" name="btn-signin" id="btn-signin" class="btn btn-green col-5 px-0" onclick="top.location.href='<?php echo site_url('member'); ?>';">เข้าสู่ระบบ</button>
            </div>

            <div class="control-group col-12 offset-0 col-sm-6 offset-sm-3">
                <button type="button" name="btn-fbsignin" id="btn-fbsignin" class="btn btn-green col-12 d-flex align-items-center justify-content-start">
                    <i class="fab fa-facebook-square"></i>
                    <span class="mx-auto">Login with Facebook</span>
                </button>
            </div>

        </form>

    </div>
</section>
<?php /* Section "#body" - End */ ?>