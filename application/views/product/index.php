<?php /* Section "#body" - Start */ ?>
<section id="body" class="product-index">
    <div class="container d-flex">

        <?php /* Content - Start */ ?>
        <div class="col-12 box box-shadow">

            <div class="wrapper col-12 offset-0 col-sm-8 offset-sm-2 d-flex flex-wrap nopadding-xs">

                <div class="box-header d-flex justify-content-center align-items-center col-12">
                    <h3 class="c-green mr-auto ml-auto leaf-left">แลกของรางวัล</h3>
                </div>
                
                <?php for( $i=1; $i<=8; $i++ ): ?>
                    <div class="items col-6 col-sm-3">
                        <div class="custom-border custom-border-2px custom-border-lightgreen bg-white">
                            <p>
                                <a href="<?php echo site_url('product/info/'.$i); ?>">
                                    <img src="img/mockup_product.png" alt="" class="img-fullwidth" />
                                </a>
                            </p>
                            <h5 class="c-brown text-center">ชื่อสินค้า <?php echo $i; ?></h5>
                            <h5 class="c-lightgreen text-center">มูลค่า <?php echo number_format(20); ?> คะแนน</h5>
                        </div>
                        <div class="text-center">
                            <a href="<?php echo site_url('product/info/'.$i); ?>" class="btn btn-green">แลกสินค้า</a>
                        </div>
                    </div>
                <?php endfor; ?>

                <?php /* Pagination - Start */ ?>
                <div class="pagination col-12">
                    <ul class="d-flex justify-content-center ml-auto mr-auto align-items-center">
                        <li class="ml-3 mr-3"><a href="javascript:void(0);" class="btn btn-green">ก่อนหน้า</a></li>
                        <?php for( $i=1; $i<=5; $i++ ): ?>
                            <li class="number ml-3 mr-3 <?php echo ( $i == 1 ? 'active' : '' ); ?>"><a href="javascript:void(0);"><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <li class="ml-3 mr-3"><a href="javascript:void(0);" class="btn btn-green">ถัดไป</a></li>
                    </ul>
                </div>
                <?php /* Pagination - End */ ?>

            </div>

        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>