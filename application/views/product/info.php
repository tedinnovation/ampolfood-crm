<?php /* Section "#body" - Start */ ?>
<section id="body" class="product-info">
    <div class="container d-flex">

        <?php /* Content - Start */ ?>
        <div class="col-12 box box-shadow d-flex flex-wrap">

            <?php /* Image - Start */ ?>
            <div id="image" class="col-12 col-sm-5 nopadding-xs">

                <div id="preview" class="custom-border custom-border-2px custom-border-lightgreen rounded slick-for bg-white">
                    <?php for( $i=1; $i<=8; $i++ ): ?>
                        <img src="img/mockup_product.png" alt="" class="img-maxwidth" />
                    <?php endfor; ?>
                </div>

                <div id="thumbnails" class="d-flex flex-wrap slick-nav">
                    <?php for( $i=1; $i<=8; $i++ ): ?>
                        <div class="items">
                            <a href="javascript:void(0);" data-image="img/mockup_product.png" class="custom-border custom-border-2px custom-border-lightgreen rounded d-flex align-items-center">
                                <img src="img/mockup_product.png" alt="" class="img-fullwidth" />
                            </a>
                        </div>
                    <?php endfor; ?>
                </div>

            </div>
            <?php /* Image - End */ ?>

            <?php /* Information - Start */ ?>
            <div id="info" class="col-12 col-sm-7 nopadding-xs">
                <h3 class="c-green">ชื่อสินค้า / ของรางวัล</h3>
                <h5>รายละเอียดสินค้า</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in lectus et libero iaculis ornare. Quisque ornare non urna nec laoreet. Quisque vitae tellus non lorem viverra condimentum.</p>
                <p>&nbsp;</p>
                <h5>คะแนนที่ใช้ <?php echo number_format(20); ?> คะแนน</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in lectus et libero iaculis ornare. Quisque ornare non urna nec laoreet. Quisque vitae tellus non lorem viverra condimentum.</p>
                <p>&nbsp;</p>
                <p class="d-flex justify-content-around">
                    <a href="<?php echo site_url('product'); ?>" class="btn btn-green">ย้อนกลับ</a>
                    <a href="<?php echo site_url('order'); ?>" id="btnAddCart" class="btn btn-green">แลกรับสิทธิ์</a>
                </p>
            </div>
            <?php /* Information - End */ ?>

            <p>&nbsp;</p>

            <?php /* Recommended - Start */ ?>
            <div id="recommended" class="col-12 d-flex flex-wrap align-items-center">
                <h3 class="c-white col-12 col-sm-4 nopadding text-xs-center text-sm-left">ของรางวัลแนะนำ</h3>
                <div class="items-wrapper col-12 col-sm-8 d-flex flex-wrap justify-content-between nopadding-xs">
                    <?php for( $i=1; $i<=4; $i++ ): ?>
                        <div class="items col-3">
                            <a href="<?php echo site_url('product/info/'.$i); ?>" class="bg-white d-flex align-items-center">
                                <img src="img/mockup_product.png" alt="" class="img-fullwidth" />
                            </a>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
            <?php /* Recommended - End */ ?>

        </div>
        <?php /* Content - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>