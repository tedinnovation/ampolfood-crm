<!DOCTYPE HTML>
<html class="<?php echo ($this->router->fetch_class()=='')?'home':$this->router->fetch_class(); ?>_<?php echo ($this->router->fetch_method()=='')?'index':$this->router->fetch_method(); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<?php echo base_url().'public/core/'; ?>" />
<title>Goodlifeforyou | สั่งสินค้าและเครื่องปรุงเพื่อสุขภาพได้ง่ายๆ ส่งทั่วไทย ง่ายแค่คลิก!!</title>

<!-- Bootstrap [CSS] - Start -->
<link rel="stylesheet" type="text/css" href="vendors/bootstrap/css/bootstrap.min.css" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="js/html5shiv.min.js"></script>
	  <script src="js/respond.min.js"></script>
	<![endif]-->
<!-- Bootstrap [CSS] - End -->

<link rel="stylesheet" type="text/css" href="css/icomoon-fonts.css" />
<link rel="stylesheet" type="text/css" href="vendors/fontawesome/css/all.css" />
<link rel="stylesheet" type="text/css" href="vendors/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="vendors/slick/slick-theme.css" />
<link rel="stylesheet" type="text/css" href="vendors/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="css/style.css?d=<?php echo date("YmdHis"); ?>" />
<link rel="stylesheet" type="text/css" href="css/responsive.css?d=<?php echo date("YmdHis"); ?>" />

</head>

<body>

<!-- Container - Start -->
<div id="container-fluid">

    <?php /* Section "header" - Start */ ?>
    <header>
        <div class="container d-flex flex-wrap align-items-center px-0">

            <?php /* Logo - Start */ ?>
            <div id="logo" class="col-4 col-sm-2 text-center">
                <a href="<?php echo site_url('home'); ?>">
                    <img src="img/logo@2x.png" alt="" width="80" class="img-fullwidth" />
                </a>
            </div>
            <?php /* Logo - End */ ?>

            <?php /* SearchBox - Start */ ?>
            <form id="searchForm" name="searchForm" method="post" enctype="multipart/form-data" action="" class="form col-6 col-sm-4 ml-auto pr-0 d-none d-sm-block text-right">
                <div class="control-group nopadding">
                    <button type="submit" id="btnSearch" name="btnSearch" class="btn"><i class="fas fa-search c-green"></i></button>
                    <input type="text" name="keywords" id="keywords" value="<?php echo set_value('keywords'); ?>" class="control-form custom-border custom-border-2px custom-border-lightgrey" />
                </div>
            </form>
            <?php /* SearchBox - End */ ?>

            <?php /* Button - Start */ ?>
            <div id="button" class="col-8 col-sm-3 text-right d-flex align-items-center justify-content-end px-3 px-sm-0">
                <a href="javascript:void(0);" id="btnSearch" class="c-green d-inline-block d-sm-none" data-toggle="modal" data-target="#searchModal"><i class="fas fa-search c-green"></i></a>
                <a href="<?php echo site_url('order'); ?>" id="btnCart" class="c-green"><i class="fal fa-shopping-basket"></i></a>
                <a href="<?php echo site_url('auth'); ?>" id="btnSignin" class="btn btn-green">เข้าสู่ระบบ</a>
                <button class="navbar-toggler d-flex d-sm-none align-items-center" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div id="nav-icon3">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
            </div>
            <?php /* Button - End */ ?>

        </div>
    </header>
    <?php /* Section "header" - End */ ?>