    <?php /* Section "footer" - Start */ ?>
    <footer class="bg-green">
        <div class="container d-flex flex-wrap">

            <div id="btnToTop-wrapper" class="col-12 justify-content-center text-center">
                <a href="javascript:void(0);" id="btnToTop">
                    <span class="fa-stack fa-2x">
                        <i class="fas fa-square-full fa-stack-1x fa-2x c-lightgreen" data-fa-transform="rotate-45"></i>
                        <i class="far fa-angle-double-up fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
            </div>

            <div id="footer-logo" class="col-6 offset-3 col-sm-2 offset-sm-1 pr-0 pl-0 text-center bg-white">
                <img src="img/logo@2x.png" alt="" class="img-fullwidth" />
            </div>

            <div id="footer-navigation" class="col-12 col-sm-8 d-flex flex-wrap">
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('home'); ?>" class="c-white">หน้าหลัก</a></p>
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('product'); ?>" class="c-white">สินค้า</a></p>
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('product'); ?>" class="c-white">สิทธิพิเศษ</a></p>
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('member/points'); ?>" class="c-white">ตรวจสอบคะแนน</a></p>
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('member/history'); ?>" class="c-white">ประวัติการแลกของรางวัล</a></p>
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('article'); ?>" class="c-white">เงื่อนไขและข้อกำหนด</a></p>
                <p class="col-6 col-sm-4 text-left"><a href="<?php echo site_url('contact'); ?>" class="c-white">ติดต่อเรา</a></p>
            </div>

            <div id="footer-socials" class="col-12 d-flex justify-content-center align-items-center">
                <a href="https://www.facebook.com/goodlifeforyou/" target="_blank" class="c-white mx-3"><i class="fab fa-facebook"></i></a>
                <a href="http://line.me/ti/p/@goodlifeforyou" target="_blank" class="c-white mx-3"><i class="fab fa-line"></i></a>
                <a href="javascript:void(0);" target="_blank" class="c-white mx-3"><i class="fab fa-youtube"></i></a>
            </div>

            <?php /*
            <div class="col-12 col-sm-8 nopadding-xs">
                <h2 class="c-lightgreen text-xs-center text-sm-left">ติดต่อเรา</h2>
                <div class="d-flex flex-wrap">

                    <div class="col-12 col-sm-6">
                        <p class="c-lightgreen text-xs-center text-sm-left">
                            เว็บไซต์  goodlifeforyou.com  คัดเลือกสินค้าและดำเนินการโดย 
                            บริษัท อำพลฟูดส์ รีเทล จำกัด
                        </p>
                        <p class="c-lightgreen text-xs-center text-sm-left">
                            บริษัท อำพลฟูดส์ รีเทล จำกัด<br>
                            เลขที่ 57 หมู่ 3 ต.กระทุ่มล้ม อ.สามพราน จ.นครปฐม 73220
                        </p>
                    </div>

                    <div class="col-12 col-sm-6">
                        <p class="c-lightgreen d-flex flex-wrap">
                            <label class="col-4 col-md-4">ศูนย์บริการลูกค้า</label>
                            <span class="col-8 col-md-8 text-left"><a href="tel:026223838" class="c-lightgreen">+66(0) 2622-3838</a></span>
                        </p>
                        <p class="c-lightgreen d-flex flex-wrap">
                            <label class="col-4 col-md-4">เวลทำการ</label>
                            <span class="col-8 col-md-8 text-left">วันจันทร์ - วันเสาร์</span>
                        </p>
                        <p class="c-lightgreen d-flex flex-wrap">
                            <label class="col-4 col-md-4">เวลา</label>
                            <span class="col-8 col-md-8 text-left">8:30 - 17:30</span>
                        </p>
                        <p class="c-lightgreen d-flex flex-wrap">
                            <label class="col-4 col-md-4">อีเมล</label>
                            <span class="col-8 col-md-8 text-left"><a href="mailto:support@goodlifeforyou.com" class="c-lightgreen">support@goodlifeforyou.com</a></span>
                        </p>
                    </div>

                </div>
            </div>
            */ ?>

            <div id="copyright-claims" class="col-12">
                <p class="c-white text-xs-center text-sm-left">©2018 Goodlifeforyou | สั่งสินค้าและเครื่องปรุงเพื่อสุขภาพได้ง่ายๆ ส่งทั่วไทย ง่ายแค่คลิก!!. All Rights Reserved. Designed by <a href="http://www.goodlifeforyou.com" target="_blank" class="c-white">GoodLifeForYou.com.</a></p>
            </div>

        </div>
    </footer>
    <?php /* Section "footer" - End */ ?>

</div>
<!-- Container - End -->

<?php /* Modal - Start */ ?>
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Looking for something?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times-circle c-green"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="searchForm" name="searchForm" method="post" enctype="multipart/form-data" action="" class="form">
                    <div class="control-group nopadding">
                        <button type="submit" id="btnSearch" name="btnSearch" class="btn"><i class="fas fa-search c-green"></i></button>
                        <input type="text" name="keywords" id="keywords" value="<?php echo set_value('keywords'); ?>" placeholder="Your keyword..." class="control-form custom-border custom-border-2px custom-border-lightgrey" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php /* Modal - End */ ?>
<script type="text/javascript" src="js/jquery-3.1.1.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.4.1.js"></script>
<script type="text/javascript" src="vendors/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="vendors/slick/slick.min.js"></script>
<script type="text/javascript" src="vendors/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="vendors/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="js/function.js"></script>

</body>
</html>