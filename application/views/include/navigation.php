    <?php /* Section "nav" - Start */ ?>
    <nav class="navbar navbar-expand-lg">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="container d-flex bg-white justify-content-center">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0);" class="nav-link dropdown-toggle text-center" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fal fa-gift c-lightgreen"></i>
                            <p class="c-green f-displayBold">แลกของรางวัล</p>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item c-green f-displayBold" href="<?php echo site_url('product'); ?>">สินค้า</a>
                            <a class="dropdown-item c-green f-displayBold" href="<?php echo site_url('product'); ?>">สิทธิพิเศษ</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url('member/points'); ?>" class="nav-link text-center">
                            <i class="fal fa-coins c-lightgreen"></i>
                            <p class="c-green f-displayBold">ตรวจสอบคะแนน</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url('member/history'); ?>" class="nav-link text-center">
                            <i class="fal fa-file-signature c-lightgreen"></i>
                            <p class="c-green f-displayBold">ประวัติการแลกของรางวัล</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url('article'); ?>" class="nav-link text-center">
                            <i class="fal fa-info-circle c-lightgreen"></i>
                            <p class="c-green f-displayBold">เงื่อนไขและข้อกำหนด</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url('contact'); ?>" class="nav-link text-center">
                            <i class="fal fa-phone c-lightgreen"></i>
                            <p class="c-green f-displayBold">ติดต่อเรา</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php /* Section "nav" - End */ ?>