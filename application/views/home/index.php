<?php /* Section "#banner" - Start */ ?>
<section id="banner">
    <div class="wrapper">
        <div class="slick">
            <?php for( $i=1; $i<=6; $i++ ): ?>
                <div>
                    <a href="javascript:void(0);"><img src="img/mockup_banner.jpg" alt="" class="img-fullwidth box-shadow" /></a>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</section>
<?php /* Section "#banner" - End */ ?>

<?php /* Section "#body" - Start */ ?>
<section id="body" class="with--banner">
    <div class="container box-shadow">

        <?php /* .box.box-1 - Start */ ?>
        <div class="box box-1">

            <div class="box-header d-flex justify-content-left justify-content-sm-center align-items-center">
                <h3 class="c-green leaf-left">สินค้าแนะนำ</h3>
                <div class="button-wrapper">
                    <a href="<?php echo site_url('product'); ?>" class="btn btn-red">ดูทั้งหมด</a>
                </div>
            </div>

            <?php /* For Desktop Version - Start */ ?>
            <div class="box-content d-none d-sm-flex flex-wrap">
                <?php for( $i=1; $i<=6; $i++ ): ?>
                    <div class="box-item col-4">
                        <a href="<?php echo site_url('product/info/'.$i); ?>" class="text-center">
                            <img src="img/mockup_product.png" alt="" class="img-fullwidth pb-0" />
                            <h2 class="c-white">ชื่อสินค้า <?php echo $i; ?></h2>
                            <h4 class="c-lightgreen mb-3">ใช้ <?php echo number_format(1000); ?> คะแนน</h4>
                        </a>
                    </div>
                <?php endfor; ?>
            </div>
            <?php /* For Desktop Version - End */ ?>

            <?php /* For Mobile Version - Start */ ?>
            <div class="box-content d-flex d-sm-none flex-wrap slick-single">
                <?php for( $i=1; $i<=6; $i++ ): ?>
                    <div class="box-item col-4">
                        <a href="<?php echo site_url('product/info/'.$i); ?>" class="text-center">
                            <img src="img/mockup_product.png" alt="" class="img-fullwidth" />
                            <h2 class="c-white">ชื่อสินค้า <?php echo $i; ?></h2>
                            <h4 class="c-brown">ใช้ <?php echo number_format(1000); ?> คะแนน</h4>
                        </a>
                    </div>
                <?php endfor; ?>
            </div>
            <?php /* For Mobile Version - End */ ?>

        </div>
        <?php /* .box.box-1 - End */ ?>

        <?php /* .box.box-2 - Start */ ?>
        <div class="box box-2">

            <div class="box-header d-flex justify-content-left justify-content-sm-center align-items-center">
                <h3 class="c-green leaf-right">สิทธิพิเศษแนะนำ</h3>
                <div class="button-wrapper">
                    <a href="<?php echo site_url('product'); ?>" class="btn btn-red">ดูทั้งหมด</a>
                </div>
            </div>

            <?php /* For Desktop Version - Start */ ?>
            <div class="box-content d-none d-sm-flex flex-wrap">
                <?php for( $i=1; $i<=6; $i++ ): ?>
                    <div class="box-item col-4">
                        <a href="<?php echo site_url('product/info/'.$i); ?>" class="text-center d-flex">
                            <div class="col-6 nopadding">
                                <img src="img/mockup_privilege.jpg" alt="" class="img-fullwidth" />
                            </div>
                            <div class="col-6 d-flex align-items-start flex-column px-2">
                                <h2 class="c-green mb-auto mt-auto w-100 text-center">
                                    <span class="small c-lightgreen">สิทธิพิเศษสำหรับสมาชิก Good Life For You</span><br />
                                    ชื่อสินค้า <?php echo $i; ?>
                                </h2>
                                <p class="c-white text-center bg-lightgreen mb-3 px-3 mx-auto"><strong>ใช้ <?php echo number_format(1000); ?> คะแนน</strong></h3>
                            </div>
                        </a>
                    </div>
                <?php endfor; ?>
            </div>
            <?php /* For Desktop Version - End */ ?>

            <?php /* For Mobile Version - Start */ ?>
            <div class="box-content d-flex d-sm-none flex-wrap slick-single">
                <?php for( $i=1; $i<=6; $i++ ): ?>
                    <div class="box-item col-4">
                        <a href="<?php echo site_url('product/info/'.$i); ?>" class="text-center d-flex">
                            <div class="col-6 nopadding">
                                <img src="img/mockup_privilege.jpg" alt="" class="img-fullwidth" />
                            </div>
                            <div class="col-6 d-flex align-items-start flex-column px-2">
                                <h2 class="c-green mb-auto mt-auto w-100 text-center">
                                    <span class="small c-lightgreen">สิทธิพิเศษสำหรับสมาชิก Good Life For You</span><br />
                                    ชื่อสินค้า <?php echo $i; ?>
                                </h2>
                                <p class="c-white text-center bg-lightgreen mb-3 px-3 mx-auto"><strong>ใช้ <?php echo number_format(1000); ?> คะแนน</strong></h3>
                            </div>
                        </a>
                    </div>
                <?php endfor; ?>
            </div>
            <?php /* For Mobile Version - End */ ?>

        </div>
        <?php /* .box.box-2 - End */ ?>

    </div>
</section>
<?php /* Section "#body" - End */ ?>