<?php
class Managecategorymodel extends CI_Model{
	
	function __construct(){
		parent::__construct();
    }
    
    public function get_categorylist( $limit=0, $offset=0 ){
        if( $limit > 0 ){
            $query = $this->db->limit( $limit );
        }

        if( $offset > 0 ){
            $query = $this->db->offset( $offset );
        }

        $query = $this->db->where( 'category_status !=','discard' )
                        ->get('rewards_category')->result_array();
        return $query;
    }

    public function count_categorylist(){
        $query = $this->db->where( 'category_status !=','discard' )
                        ->count('rewards_category');
        return $query;
    }

    public function get_categoryinfo_byid( $categoryid=0 ){
        $query = $this->db->where( 'id', $categoryid )
                        ->get( 'rewards_category' )->row_array();
        return $query;
    }

    public function reOrder(){
        $lists = $this->get_categorylist();
        if( isset( $lists ) && count( $lists ) ){
            $i=0;
            foreach( $lists as $list ){
                $i++;
                $this->db->set( 'category_order', $i );
                $this->db->where( 'id', $list['id'] );
                $this->db->update('rewards_category');
            }
        }
    }

    
}
?>