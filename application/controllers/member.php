<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

    var $_data;
    public function __construct(){
        parent::__construct();
        $this->load->model('member/membermodel');
    }
    

    public function index(){
        
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('member/index');
        $this->load->view('include/footer');

    }

    public function points(){
        
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('member/points');
        $this->load->view('include/footer');
        
    }

    public function pointinfo( $id=0 ){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('member/pointinfo');
        $this->load->view('include/footer');

    }

    public function history(){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('member/history');
        $this->load->view('include/footer');

    }

    public function historyinfo( $id=0 ){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('member/historyinfo');
        $this->load->view('include/footer');
        
    }

}

/* End of file Member.php */