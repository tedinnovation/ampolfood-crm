<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
    }
    

    public function index(){
        
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('contact/index');
        $this->load->view('include/footer');

    }

    public function member(){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('contact/member');
        $this->load->view('include/footer');
        
    }

}

/* End of file Contact.php */
