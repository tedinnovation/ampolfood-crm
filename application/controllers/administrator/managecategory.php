<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Managecategory extends CI_CONTROLLER{
	var $_data = array();
	var $seq;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->load->model('administrator/managecategory/managecategorymodel');
		
		$this->admin_library->forceLogin();
		
		$this->admin_model->set_menu_key('87846406d4a3008fb1e0b17928ea6787');
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("การจัดการของรางวัล",'icon-list-alt');
	}

	public function index(){
		
		if(!$this->admin_model->check_permision("r")){
			show_error("สิทธิการเข้าถึงของคุณไม่ถูกต้อง.");
		}
		
		$this->admin_model->set_menu_key('0f8d20c77f003aceafc14215e6b9cd20');
		$this->admin_model->set_detail('รายการหมวดหมู่ของรางวัล');
		
		/* Get Data Table - Start */
		$perpage = 10;
		if($offset>1){
			$offset = ($offset*$perpage)-$perpage;
			$this->seq = $offset;
		}else{
			$offset = 0;
		}
		$this->admin_model->set_dataTable($this->managecategorymodel->get_categorylists($perpage, $offset));
		$totalrows = $this->managecategorymodel->count_categorylists();
		/* Get Data Table - End */
		
		$this->admin_model->set_top_button('เพิ่มหมวดหมู่','managecategory/create','icon-plus','btn-success','w');
		
		$this->admin_model->set_column('id','ลำดับ','5%','icon-list');
		$this->admin_model->set_column('category_icon','ไอคอน','5%','icon-info-circle');
		$this->admin_model->set_column('category_name_th','ชื่อหมวดหมู่ (ไทย / En)','25%','icon-font');
		$this->admin_model->set_column('category_order','การจัดลำดับ','15%','icon-sort');
		$this->admin_model->set_column('category_status','สถานะการแสดงผล','15%','icon-eye-slash');
		$this->admin_model->set_action_button('แก้ไข','managecategory/update/[id]','icon-pencil-square','btn-primary','w');
		$this->admin_model->set_action_button('ลบข้อมูล','managecategory/delete/[id]','icon-trash','btn-danger','d');
		$this->admin_model->set_action_button('รายการของรางวัล','managerewards/index/[id]','icon-th-list','btn-default','r');
		$this->admin_model->set_column_callback('id','show_seq');
		$this->admin_model->set_column_callback('category_icon','show_icon');
		$this->admin_model->set_column_callback('category_name_th','show_name');
		$this->admin_model->set_column_callback('category_order','show_order');
		$this->admin_model->set_column_callback('category_status','show_status');
		
		$this->admin_model->set_pagination("managecategory/index",$totalrows,$perpage,4);
		$this->admin_model->make_list();
				
		$this->admin_library->output();
		
	}
	
	public function create(){
		
		if(!$this->admin_model->check_permision("w")){
			show_error("สิทธิการเข้าถึงของคุณไม่ถูกต้อง.");
		}
		
		$this->admin_model->set_menu_key('0f8d20c77f003aceafc14215e6b9cd20');
		$this->admin_model->set_detail('เพิ่มหมวดหมู่ของรางวัล');
		
		$this->form_validation->set_rules('category_icon','ไอคอน','trim|required');
		$this->form_validation->set_rules('category_name_th','ชื่อหมวดหมู่ (ไทย)','trim|required');
		$this->form_validation->set_rules('category_name_en','ชื่อหมวดหมู่ (En)','trim|required');
		$this->form_validation->set_rules('category_status','สถานะการแสดงผล','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->admin_library->view('managecategory/create', $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$message = $this->managecategorymodel->create();
						
			$this->session->set_flashdata($message['status'], $message['text']);
			admin_redirect('managecategory/index');
			
		}
		
	}
	
	public function update( $categoryid=0 ){
		
		if(!$this->admin_model->check_permision("w")){
			show_error("สิทธิการเข้าถึงของคุณไม่ถูกต้อง.");
		}
		
		$this->admin_model->set_menu_key('0f8d20c77f003aceafc14215e6b9cd20');
		$this->admin_model->set_detail('แก้ไขหมวดหมู่ของรางวัล');
		
		$this->form_validation->set_rules('category_icon','ไอคอน','trim|required');
		$this->form_validation->set_rules('category_name_th','ชื่อหมวดหมู่ (ไทย)','trim|required');
		$this->form_validation->set_rules('category_name_en','ชื่อหมวดหมู่ (En)','trim|required');
		$this->form_validation->set_rules('category_status','สถานะการแสดงผล','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->_data['info'] = $this->managecategorymodel->get_categoryinfo_byid( $categoryid );
			
			$this->admin_library->view('managecategory/update', $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$message = $this->managecategorymodel->create();
						
			$this->session->set_flashdata($message['status'], $message['text']);
			admin_redirect('managecategory/index');
			
		}
	}
	
	public function setorder( $movement='up', $categoryid=0 ){
		
		$message = array();
		
		$message = $this->managecategorymodel->setOrder( $movement, $categoryid );
		
		$this->session->set_flashdata($message['status'],$message['text']);
		admin_redirect('managecategory/index');
		
	}
	
	public function delete( $setto='discard', $categoryid=0 ){
		
		$message = array();
		
		$message = $this->managecategorymodel->setStatus( $setto, $categoryid );
		
		$this->session->set_flashdata($message['status'],$message['text']);
		admin_redirect('managecategory/index');
		
	}
	
	/* Default function - Start */
	public function show_seq($text, $row){
		$this->seq++;
		return $this->seq;
	}
	
	public function show_icon( $text, $row ){
		if( $text != '' ){
			return '<i class="'.$text.'"></i>';
		}else{
			return '';
		}
	}

	public function show_name( $text, $row ){
		return $row['category_name_th'].' / '.$row['category_name_en'];
	}
	
	public function show_status($text, $row){
		switch($text){
			case 'approved'	: return '<span class="label label-success"><i class="icon-unlock"></i> แสดงผล</span>'; break;
			case 'pending'	: return '<span class="label label-inverse"><i class="icon-lock"></i> ไม่แสดงผล</span>'; break;
			default : return 'ไม่มีสถานะ';
		}
	}
	
	public function show_order($text, $row){
		$text = $text.' ';
		$text .= '(<a href="'.admin_url('managecategory/setorder/up/'.$row['id']).'"><i class="icon-chevron-up"></i> เลื่อนขึ้น</a>';
		$text .= ' | ';
		$text .= '<a href="'.admin_url('managecategory/setorder/down/'.$row['id']).'"><i class="icon-chevron-down"></i> เลื่อนลง</a>)';
		return $text;
	}
	/* Default function -  End */

}