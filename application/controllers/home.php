<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    var $_data;
	function __construct(){
		parent::__construct();
	}
	
	function index(){
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('home/index');
        $this->load->view('include/footer');
	}
}

?>