<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
    
    var $_data;
    public function __construct(){
        parent::__construct();
        $this->load->model('order/ordermodel');
    }
    

    public function index(){
        
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('order/index');
        $this->load->view('include/footer');

    }

    public function summary(){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('order/summary');
        $this->load->view('include/footer');

    }

    public function confirm(){
        redirect('order/thankyou');
    }

    public function thankyou(){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('order/thankyou');
        $this->load->view('include/footer');
        
    }

}

/* End of file Order.php */
