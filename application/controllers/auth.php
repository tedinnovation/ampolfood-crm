<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    var $_data;
    public function __construct(){
        parent::__construct();
        $this->load->model('auth/authmodel');
    }
    

    public function index(){
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('auth/index');
        $this->load->view('include/footer');
    }

    public function signout(){
        redirect('home');
    }

}

/* End of file Auth.php */
