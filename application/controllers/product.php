<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    var $_data;
    public function __construct(){
        parent::__construct();
        $this->load->model('product/productmodel');
    }
    

    public function index(){
        
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('product/index');
        $this->load->view('include/footer');

    }

    public function info( $id=0 ){

        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('product/info');
        $this->load->view('include/footer');
        
    }

}

/* End of file Product.php */
