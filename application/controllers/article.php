<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

    
    public function __construct(){
        parent::__construct();
    }
    

    public function index(){
        $this->load->view('include/header');
        $this->load->view('include/navigation');
        $this->load->view('article/index');
        $this->load->view('include/footer');
    }

}

/* End of file Arrticle.php */
